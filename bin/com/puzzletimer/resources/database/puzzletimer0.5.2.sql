BEGIN TRANSACTION;

-- puzzles

INSERT INTO PUZZLE VALUES
    (15, '8x8x8-CUBE',        '8x8x8 cube'),
    (16, '9x9x9-CUBE',        '9x9x9 cube');

-- colors

INSERT INTO COLOR VALUES
    ('8x8x8-CUBE',    0, 'FACE-U',          'Face U',          255, 255, 255, 255, 255, 255),
    ('8x8x8-CUBE',    1, 'FACE-D',          'Face D',          255, 234,   0, 255, 234,   0),
    ('8x8x8-CUBE',    2, 'FACE-L',          'Face L',          255,  85,   0, 255,  85,   0),
    ('8x8x8-CUBE',    3, 'FACE-R',          'Face R',          212,  17,  17, 212,  17,  17),
    ('8x8x8-CUBE',    4, 'FACE-F',          'Face F',            0, 153,   0,   0, 153,   0),
    ('8x8x8-CUBE',    5, 'FACE-B',          'Face B',            0,  13, 153,   0,  13, 153),
    ('9x9x9-CUBE',    0, 'FACE-U',          'Face U',          255, 255, 255, 255, 255, 255),
    ('9x9x9-CUBE',    1, 'FACE-D',          'Face D',          255, 234,   0, 255, 234,   0),
    ('9x9x9-CUBE',    2, 'FACE-L',          'Face L',          255,  85,   0, 255,  85,   0),
    ('9x9x9-CUBE',    3, 'FACE-R',          'Face R',          212,  17,  17, 212,  17,  17),
    ('9x9x9-CUBE',    4, 'FACE-F',          'Face F',            0, 153,   0,   0, 153,   0),
    ('9x9x9-CUBE',    5, 'FACE-B',          'Face B',            0,  13, 153,   0,  13, 153);

-- scramblers

INSERT INTO SCRAMBLER VALUES
    (-1, '8x8x8-CUBE-IMPORTER',                          '8x8x8-CUBE',   'Importer scrambler',                            TRUE),
    ( 0, '8x8x8-CUBE-RANDOM',                            '8x8x8-CUBE',   'Random scrambler',                             FALSE),
    (-1, '9x9x9-CUBE-IMPORTER',                          '9x9x9-CUBE',   'Importer scrambler',                            TRUE),
    ( 0, '9x9x9-CUBE-RANDOM',                            '9x9x9-CUBE',   'Random scrambler',                             FALSE);

-- version

UPDATE CONFIGURATION SET VALUE = '0.5.2' WHERE KEY = 'VERSION';

COMMIT;

