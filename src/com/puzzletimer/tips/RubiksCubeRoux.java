package com.puzzletimer.tips;

import static com.puzzletimer.Internationalization._;

import com.puzzletimer.models.Scramble;
import com.puzzletimer.solvers.RubiksCubeRouxSolver;

public class RubiksCubeRoux implements Tip {

	@Override
	public String getTipId() {
		return "RUBIKS-CUBE-OPTIMAL-ROUX";
	}

	@Override
	public String getPuzzleId() {
		return "RUBIKS-CUBE";
	}

	@Override
	public String getTipDescription() {
		return _("tip.RUBIKS-CUBE-OPTIMAL-ROUX");
	}

	@Override
	public String getTip(Scramble scramble) {
		String[] scr = scramble.getSequence();
		StringBuilder tip = new StringBuilder();
		String[] face = {_("tip.RUBIKS-CUBE-OPTIMAL-ROUX.roux_on_d"), _("tip.RUBIKS-CUBE-OPTIMAL-ROUX.roux_on_u"),
				_("tip.RUBIKS-CUBE-OPTIMAL-ROUX.roux_on_l"), _("tip.RUBIKS-CUBE-OPTIMAL-ROUX.roux_on_r"),
				_("tip.RUBIKS-CUBE-OPTIMAL-ROUX.roux_on_f"), _("tip.RUBIKS-CUBE-OPTIMAL-ROUX.roux_on_b")};
		for(int i=0; i<6; i++) {
			tip.append(face[i]+"\n");
			for (int j=0; j<4; j++) {
				tip.append("  " + RubiksCubeRouxSolver.solve(scr, i, j) + "\n");
			}
			tip.append("\n");
		}
		return tip.toString().trim();
	}

	@Override
    public String toString() {
        return getTipDescription();
    }
}
