package com.puzzletimer.tips;

import static com.puzzletimer.Internationalization._;

import com.puzzletimer.models.Scramble;
import com.puzzletimer.solvers.RubiksCubePetrusSolver;

public class RubiksCubePetrus implements Tip {

	@Override
	public String getTipId() {
		return "RUBIKS-CUBE-OPTIMAL-PETRUS";
	}

	@Override
	public String getPuzzleId() {
		return "RUBIKS-CUBE";
	}

	@Override
	public String getTipDescription() {
		return _("tip.RUBIKS-CUBE-OPTIMAL-PETRUS");
	}

	@Override
	public String getTip(Scramble scramble) {
		String[] scr = scramble.getSequence();
		StringBuilder tip = new StringBuilder();
		String[] face = {_("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_ulf"), _("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_ulb"),
				_("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_urf"), _("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_urb"),
				_("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_dlf"), _("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_dlb"),
				_("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_drf"), _("tip.RUBIKS-CUBE-OPTIMAL-PETRUS.petrus_on_drb")};
		for(int i=0; i<8; i++) {
			tip.append(face[i]+":\n");
			for (String solution : RubiksCubePetrusSolver.solve(scr, i)) {
				tip.append(" "+solution +"\n");
			}
			tip.append("\n");
		}
		return tip.toString().trim();
	}

	@Override
    public String toString() {
        return getTipDescription();
    }
}
