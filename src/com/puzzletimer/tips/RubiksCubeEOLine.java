package com.puzzletimer.tips;

import static com.puzzletimer.Internationalization._;
import com.puzzletimer.models.Scramble;
import com.puzzletimer.solvers.RubiksCubeEOLineSolver;
import com.puzzletimer.solvers.RubiksCubeSolver.State;
import com.puzzletimer.util.StringUtils;

public class RubiksCubeEOLine implements Tip {
	private static State x = new State(
            new byte[] { 3, 2, 6, 7, 0, 1, 5, 4 },
            new byte[] { 2, 1, 2, 1, 1, 2, 1, 2 },
            new byte[] { 7, 5, 9, 11, 6, 2, 10, 3, 4, 1, 8, 0 },
            new byte[] { 0, 0, 0,  0, 1, 0,  1, 0, 1, 0, 1, 0 });
    private static State y = new State(
            new byte[] { 3, 0, 1, 2, 7, 4, 5, 6 },
            new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new byte[] { 3, 0, 1, 2, 7, 4, 5, 6, 11, 8, 9, 10 },
            new byte[] { 1, 1, 1, 1, 0, 0, 0, 0,  0, 0, 0,  0 });
    private static State z = new State(
            new byte[] { 4, 0, 3, 7, 5, 1, 2, 6 },
            new byte[] { 1, 2, 1, 2, 2, 1, 2, 1 },
            new byte[] { 8, 4, 6, 10, 0, 7, 3, 11, 1, 5, 2, 9 },
            new byte[] { 1, 1, 1,  1, 1, 1, 1, 1,  1, 1, 1, 1 });
	
	@Override
	public String getTipId() {
        return "RUBIKS-CUBE-OPTIMAL-EOLINE";
    }

	@Override
	public String getPuzzleId() {
		return "RUBIKS-CUBE";
	}

	@Override
	public String getTipDescription() {
		return _("tip.RUBIKS-CUBE-OPTIMAL-EOLINE");
	}

	@Override
	public String getTip(Scramble scramble) {
		State state = State.id.applySequence(scramble.getSequence());
		
		StringBuilder tip = new StringBuilder();
		
		State stateU1 = z.multiply(z).multiply(state).multiply(z).multiply(z);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_u_lr") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateU1)) {
            tip.append("  z2 " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateU2 = y.multiply(y).multiply(y).multiply(stateU1).multiply(y);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_u_fb") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateU2)) {
            tip.append("  z2 y " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateD1 = state;
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_d_lr") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateD1)) {
            tip.append("  " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateD2 = y.multiply(y).multiply(y).multiply(state).multiply(y);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_d_fb") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateD2)) {
            tip.append("  y " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateL1 = z.multiply(state).multiply(z).multiply(z).multiply(z);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_l_ud") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateL1)) {
            tip.append("  z' " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateL2 = y.multiply(y).multiply(y).multiply(stateL1).multiply(y);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_l_fb") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateL2)) {
            tip.append("  z' y " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateR1 = z.multiply(z).multiply(z).multiply(state).multiply(z);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_r_ud") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateR1)) {
            tip.append("  z " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateR2 = y.multiply(y).multiply(y).multiply(stateR1).multiply(y);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_r_fb") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateR2)) {
            tip.append("  z y " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateF1 = x.multiply(state).multiply(x).multiply(x).multiply(x);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_f_lr") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateF1)) {
            tip.append("  x' " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateF2 = y.multiply(y).multiply(y).multiply(stateF1).multiply(y);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_f_ud") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateF2)) {
            tip.append("  x' y " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateB1 = x.multiply(x).multiply(x).multiply(state).multiply(x);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_b_lr") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateB1)) {
            tip.append("  x " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		
		State stateB2 = y.multiply(y).multiply(y).multiply(stateB1).multiply(y);
		tip.append(_("tip.RUBIKS-CUBE-OPTIMAL-EOLINE.eoline_on_b_ud") + ":\n");
		for (String[] solution : RubiksCubeEOLineSolver.solve(stateB2)) {
            tip.append("  x y " + StringUtils.join(" ", solution) + "\n");
        }
		tip.append("\n");
		return tip.toString().trim();
	}
	
	@Override
    public String toString() {
        return getTipDescription();
    }
}
