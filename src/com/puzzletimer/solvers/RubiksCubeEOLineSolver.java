package com.puzzletimer.solvers;

import java.util.ArrayList;

import com.puzzletimer.solvers.RubiksCubeSolver.State;

public class RubiksCubeEOLineSolver {
	// moves
	private static State[] moves = new State[6];
	private static int goalEp;
    static {
    	String[] turn = {"U", "D", "L", "R", "F", "B"};
        for (int i = 0; i < 6; i++) {
            moves[i] = State.moves.get(turn[i]);
        }
        int[] ind = stateToIndices(State.id);
        goalEp = ind[0] * 2 + ind[1];
    }

    // move tables
    private static short[][] eom = new short[2048][6];
	private static short[][] epm = new short[132][6];
	// distance tables
	private static byte[] eod = new byte[2048];
	private static byte[] epd = new byte[132];

	private static boolean ini = false;
	private static void init() {
		if(ini)return;
		// edges orientation
		for(int i=0; i<2048; i++){
			State state = new State(new byte[8], new byte[8], new byte[12],
					IndexMapping.indexToZeroSumOrientation(i, 2, 12));
			for(int j=0; j<6; j++) {
				State next = state.multiply(moves[j]);
				eom[i][j] = (short) IndexMapping.zeroSumOrientationToIndex(
						next.edgesOrientation, 2);
			}
		}
		// edges permutation
		for(int i=0; i<66; i++){
			for(int j=0; j<2; j++){
				State state = indicesToState(new int[]{i, j, 0});
				for(int k=0; k<6; k++){
					int[] indices = stateToIndices(state.multiply(moves[k]));
					epm[i*2+j][k] = (short) (indices[0] * 2 + indices[1]);
				}
			}
		}
		// distance
		for(int i=1; i<2048; i++) eod[i] = -1;
		eod[0] = 0;
		int d;
		for(d=0; d<7; d++){
			//n=0;
			for(int i=0; i<2048; i++)
				if(eod[i] == d)
					for(int j=0; j<6; j++)
						for(int y=i, m=0; m<3; m++){
							y=eom[y][j];
							if(eod[y]==-1){
								eod[y]=(byte) (d+1);
								//n++;
							}
						}
			//System.out.println(d+" "+n);
		}
		for(int i=0; i<132; i++) epd[i] = -1;
		epd[goalEp] = 0;
		for(d=0; d<4; d++){
			//n=0;
			for(int i=0; i<132; i++)
				if(epd[i] == d)
					for(int j=0; j<6; j++){
						int y=i;
						for(int m=0; m<3; m++){
							y=epm[y][j];
							if(epd[y]==-1){
								epd[y]=(byte) (d+1);
								//n++;
							}
						}
					}
			//System.out.println(d+" "+n);
		}
		ini = true;
	}
	
	private static int[] stateToIndices(State state) {
        // edges
        boolean[] selectedEdges = {
            false, false, false, false,
            false, false, false, false,
            true,  false, true,  false,
        };

        byte[] edgesMapping = {
           -1, -1, -1, -1,
           -1, -1, -1, -1,
            0, -1,  1, -1,
        };

        boolean[] edgesCombination = new boolean[12];
        for (int i = 0; i < 12; i++) {
            edgesCombination[i] = selectedEdges[state.edgesPermutation[i]];
        }
        int edgesCombinationIndex =
            IndexMapping.combinationToIndex(edgesCombination, 2);

        byte[] edgesPermutation = new byte[2];
        byte[] edgesOrientation = new byte[2];
        int next = 0;
        for (int i = 0; i < 12; i++) {
            if (edgesCombination[i]) {
                edgesPermutation[next] = edgesMapping[state.edgesPermutation[i]];
                edgesOrientation[next++] = state.edgesOrientation[i];
            }
        }
        int edgesPermutationIndex =
            IndexMapping.permutationToIndex(edgesPermutation);
        int edgesOrientationIndex =
            IndexMapping.orientationToIndex(edgesOrientation, 2);

        return new int[] {
            edgesCombinationIndex,
            edgesPermutationIndex,
            edgesOrientationIndex,
        };
    }

	private static State indicesToState(int[] indices) {
        boolean[] combination =
            IndexMapping.indexToCombination(indices[0], 2, 12);
        byte[] permutation =
            IndexMapping.indexToPermutation(indices[1], 2);
        byte[] orientation =
            IndexMapping.indexToOrientation(indices[2], 2, 2);

        byte[] selectedEdges = { 8, 10 };
        int nextSelectedEdgeIndex = 0;
        byte[] otherEdges = { 0, 1, 2, 3, 4, 5, 6, 7, 9, 11 };
        int nextOtherEdgeIndex = 0;

        byte[] edgesPermutation = new byte[12];
        byte[] edgesOrientation = new byte[12];
        for (int i = 0; i < edgesPermutation.length; i++) {
            if (combination[i]) {
                edgesPermutation[i] = selectedEdges[permutation[nextSelectedEdgeIndex]];
                edgesOrientation[i] = orientation[nextSelectedEdgeIndex];
                nextSelectedEdgeIndex++;
            } else {
                edgesPermutation[i] = otherEdges[nextOtherEdgeIndex];
                edgesOrientation[i] = 0;
                nextOtherEdgeIndex++;
            }
        }

        return new State(
            State.id.cornersPermutation,
            State.id.cornersOrientation,
            edgesPermutation,
            edgesOrientation);
    }
	
	public static ArrayList<String[]> solve(State state) {
    	init();

        int[] indices = stateToIndices(state);
        int ep = indices[0] * 2 + indices[1];
        int eo = IndexMapping.zeroSumOrientationToIndex(state.edgesOrientation, 2);

        ArrayList<String[]> solutions = new ArrayList<String[]>();

        for (int depth = 0; ; depth++) {
            int[] path = new int[depth];

            search(ep, eo, depth, path, solutions, -1);

            if (solutions.size() > 0) {
                return solutions;
            }
        }
    }
	
	private static String[] moveNames = {
        "U", "U2", "U'",
        "D", "D2", "D'",
        "L", "L2", "L'",
        "R", "R2", "R'",
        "F", "F2", "F'",
        "B", "B2", "B'",
    };
	private static void search(int ep, int eo, int d,
            int[] path, ArrayList<String[]> sol, int lf) {
        if (d == 0) {
            if (ep == goalEp && eo == 0) {
                String[] sequence = new String[path.length];
                for (int i = 0; i < sequence.length; i++) {
                    sequence[i] = moveNames[path[i]];
                }
                sol.add(sequence);
            }
            return;
        }

        if (epd[ep] > d || eod[eo] > d) return;

        for (int i = 0; i < 6; i++) {
        	if(i != lf) {
        		int y = ep, s = eo;
        		for(int j = 0; j < 3; j++){
        			y = epm[y][i]; s = eom[s][i];
        			path[path.length - d] = i * 3 + j;
        			search(y, s, d - 1, path, sol, i);
        		}
        	}
        }
    }
}
