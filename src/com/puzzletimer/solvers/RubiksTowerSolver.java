package com.puzzletimer.solvers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class RubiksTowerSolver {
    public static class State {
        public byte[] orientation;
        public byte[] edgesPermutation;
        public byte[] cornersPermutation;

        public State(byte[] orientation, byte[] edgesPermutation, byte[] cornersPermutation) {
            this.orientation = orientation;
            this.edgesPermutation = edgesPermutation;
            this.cornersPermutation = cornersPermutation;
        }

        public State multiply(State move) {
            byte[] orientation = new byte[8];
            byte[] edgesPermutation = new byte[8];
            byte[] cornersPermutation = new byte[8];

            for (int i = 0; i < 8; i++) {
                orientation[i] = (byte) ((this.orientation[move.edgesPermutation[i]] + move.orientation[i]) % 3);
                edgesPermutation[i] = this.edgesPermutation[move.edgesPermutation[i]];
                cornersPermutation[i] = this.cornersPermutation[move.cornersPermutation[i]];
            }

            return new State(orientation, edgesPermutation, cornersPermutation);
        }
    }

    private boolean initialized;

    private State[] moves1;
    private String[] moveNames1 = {
    		"Uw", "Uw2", "Uw'",
    		"Dw", "Dw2", "Dw'",
    		"Lw", "Lw2", "Lw'",
    		"Rw", "Rw2", "Rw'",
    		"Fw", "Fw2", "Fw'",
    		"Bw", "Bw2", "Bw'",
    };
    private State[] moves2;
    private String[] moveNames2 = {
            "Uw", "Uw2", "Uw'",
            "Dw", "Dw2", "Dw'",
            "Lw2",
            "Rw2",
            "Fw2",
            "Bw2",
            "U",  "U2",  "U'",
            "D",  "D2",  "D'",
        };
    private int[] faces2 = {3, 3, 1, 1, 1, 1, 3, 3};
    private int[] index2 = {0, 3, 6, 7, 8, 9, 10, 13};
    private short[][] om = new short[2187][6];
    private char[][] epm = new char[40320][8];
    private byte[][] ecm = new byte[70][8];
    private char[][] cpm = new char[40320][8];
    private byte[][] ccm = new byte[70][8];
    private byte[] od;
    private byte[][] epd;
    private byte[][] cpd;

    public RubiksTowerSolver() {
        initialized = false;
    }

    private void initialize() {
    	long tm = System.currentTimeMillis();
        // moves
        State moveUw = new State(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 3, 0, 1, 2, 4, 5, 6, 7 }, new byte[] { 3, 0, 1, 2, 4, 5, 6, 7 });
        State moveDw = new State(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 5, 6, 7, 4 }, new byte[] { 0, 1, 2, 3, 5, 6, 7, 4 });
        State moveLw = new State(new byte[] { 2, 0, 0, 1, 1, 0, 0, 2 }, new byte[] { 4, 1, 2, 0, 7, 5, 6, 3 }, new byte[] { 4, 1, 2, 0, 7, 5, 6, 3 });
        State moveRw = new State(new byte[] { 0, 1, 2, 0, 0, 2, 1, 0 }, new byte[] { 0, 2, 6, 3, 4, 1, 5, 7 }, new byte[] { 0, 2, 6, 3, 4, 1, 5, 7 });
        State moveFw = new State(new byte[] { 0, 0, 1, 2, 0, 0, 2, 1 }, new byte[] { 0, 1, 3, 7, 4, 5, 2, 6 }, new byte[] { 0, 1, 3, 7, 4, 5, 2, 6 });
        State moveBw = new State(new byte[] { 1, 2, 0, 0, 2, 1, 0, 0 }, new byte[] { 1, 5, 2, 3, 0, 4, 6, 7 }, new byte[] { 1, 5, 2, 3, 0, 4, 6, 7 });
        State moveU  = new State(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 }, new byte[] { 3, 0, 1, 2, 4, 5, 6, 7 });
        State moveD  = new State(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 }, new byte[] { 0, 1, 2, 3, 5, 6, 7, 4 });

        moves1 = new State[] {moveUw, moveDw, moveLw, moveRw, moveFw, moveBw};

        moves2 = new State[] {
            moveUw,
            moveDw,
            moveLw.multiply(moveLw),
            moveRw.multiply(moveRw),
            moveFw.multiply(moveFw),
            moveBw.multiply(moveBw),
            moveU,
            moveD,
        };

        // move tables
        for (int i = 0; i < 2187; i++) {
            State state = new State(IndexMapping.indexToZeroSumOrientation(i, 3, 8), new byte[8], new byte[8]);
            for (int j = 0; j < 6; j++) {
            	om[i][j] = (short) IndexMapping.zeroSumOrientationToIndex(
            			state.multiply(moves1[j]).orientation, 3);
            }
        }

        for (int i = 0; i < 40320; i++) {
            State state = new State(new byte[8], IndexMapping.indexToPermutation(i, 8), new byte[8]);
            for (int j = 0; j < 8; j++) {
                epm[i][j] = (char) IndexMapping.permutationToIndex(
                        state.multiply(moves2[j]).edgesPermutation);
            }
        }

        for (int i = 0; i < 70; i++) {
            boolean[] combination = IndexMapping.indexToCombination(i, 4, 8);
            byte[] edges = new byte[8];
            byte nextTop = 0;
            byte nextBottom = 4;
            for (int j = 0; j < 8; j++) {
                if (combination[j]) {
                    edges[j] = nextTop++;
                } else {
                    edges[j] = nextBottom++;
                }
            }
            State state = new State(new byte[8], edges, new byte[8]);
            for (int j = 0; j < 8; j++) {
                State result = state.multiply(moves2[j]);
                boolean[] isTopEdge = new boolean[8];
                for (int k = 0; k < 8; k++) {
                    isTopEdge[k] = result.edgesPermutation[k] < 4;
                }
                ecm[i][j] = (byte) IndexMapping.combinationToIndex(isTopEdge, 4);
            }
        }

        for (int i = 0; i < 40320; i++) {
            State state = new State(new byte[8], new byte[8], IndexMapping.indexToPermutation(i, 8));
            for (int j = 0; j < 8; j++) {
                cpm[i][j] = (char) IndexMapping.permutationToIndex(
                        state.multiply(moves2[j]).cornersPermutation);
            }
        }

        for (int i = 0; i < 70; i++) {
            boolean[] combination = IndexMapping.indexToCombination(i, 4, 8);
            byte[] corners = new byte[8];
            byte nextTop = 0;
            byte nextBottom = 4;
            for (int j = 0; j < 8; j++) {
                if (combination[j]) {
                    corners[j] = nextTop++;
                } else {
                    corners[j] = nextBottom++;
                }
            }
            State state = new State(new byte[8], new byte[8], corners);
            for (int j = 0; j < 8; j++) {
                State result = state.multiply(moves2[j]);
                boolean[] isTopCorner = new boolean[8];
                for (int k = 0; k < 8; k++) {
                    isTopCorner[k] = result.cornersPermutation[k] < 4;
                }
                ccm[i][j] = (byte) IndexMapping.combinationToIndex(isTopCorner, 4);
            }
        }
        System.out.println(System.currentTimeMillis()-tm);
        tm = System.currentTimeMillis();
        // prune tables
        od = new byte[2187];
        for (int i = 0; i < 2187; i++)
            od[i] = -1;
        od[0] = 0;
        int depth;
        for(depth = 0; depth < 6; depth++)
            for (int i = 0; i < 2187; i++)
                if (od[i] == depth)
                    for (int j = 0; j < 6; j++) {
                    	int y = i;
                    	for(int k = 0; k < 3; k++) {
                    		y = om[y][j];
                    		if (od[y] < 0)
                                od[y] = (byte) (depth + 1);
                    	}
                    }

        epd = new byte[40320][70];
        for (int i = 0; i < 40320; i++)
            for (int j = 0; j < 70; j++)
                epd[i][j] = -1;
        epd[0][0] = 0;
        for(depth = 0; depth < 15; depth++)
            for (int i = 0; i < 40320; i++)
                for (int j = 0; j < 70; j++)
                    if (epd[i][j] == depth)
                        for (int k = 0; k < 8; k++) {
                        	int y=i, s=j;
                        	for(int m = 0; m < faces2[k]; m++) {
                        		y = epm[y][k];
                        		s = ccm[s][k];
                        		if(epd[y][s] < 0)
                        			epd[y][s] = (byte) (depth + 1);
                        	}
                        }

        cpd = new byte[40320][70];
        for (int i = 0; i < 40320; i++)
            for (int j = 0; j < 70; j++)
                cpd[i][j] = -1;
        cpd[0][0] = 0;
        depth = 0;
        for(depth = 0; depth < 13; depth++)
            for (int i = 0; i < 40320; i++)
                for (int j = 0; j < 70; j++)
                    if (cpd[i][j] == depth)
                        for (int k = 0; k < 8; k++) {
                        	int y = i, s = j;
                        	for(int m = 0; m < faces2[k]; m++) {
                        		y = cpm[y][k];
                        		s = ecm[s][k];
                        		if (cpd[y][s] < 0)
                                    cpd[y][s] = (byte) (depth + 1);
                        	}
                        }
        System.out.println(System.currentTimeMillis()-tm);
        
        initialized = true;
    }

    public String[] solve(State state) {
        if (!initialized) {
            initialize();
        }

        // orientation
        int orientation =
            IndexMapping.zeroSumOrientationToIndex(state.orientation, 3);

        for (int depth = 0; ; depth++) {
            ArrayList<Integer> solution = new ArrayList<Integer>();
            if (search(orientation, depth, solution, -1)) {
                ArrayList<String> sequence = new ArrayList<String>();

                State state2 = state;
                for (int moveIndex : solution) {
                    sequence.add(moveNames1[moveIndex]);
                    for(int i=0; i<moveIndex%3; i++)
                    state2 = state2.multiply(moves1[moveIndex/3]);
                }

                String[] solution2 = solve2(state2);
                for (String move : solution2) {
                    sequence.add(move);
                }

                String[] sequenceArray = new String[sequence.size()];
                sequence.toArray(sequenceArray);

                return sequenceArray;
            }
        }
    }

    private boolean search(int o, int depth, ArrayList<Integer> sol, int lf) {
        if (depth == 0) return o == 0;

        if (od[o] <= depth) {
            for (int i = 0; i < 6; i++) {
                if (i == lf) {
                    continue;
                }
                int y = o;
                for(int j = 0; j < 3; j++){
                	y = om[y][i];
                	if (search(y, depth - 1, sol, i)) {
                		sol.add(i*3+j);
                		return true;
                	}
                }
            }
        }

        return false;
    }

    private String[] solve2(State state) {
        // edges permutation
        int edgesPermutation =
            IndexMapping.permutationToIndex(state.edgesPermutation);

        // edges combination
        boolean[] isTopEdge = new boolean[8];
        for (int k = 0; k < isTopEdge.length; k++) {
            isTopEdge[k] = state.edgesPermutation[k] < 4;
        }
        int edgesCombination = IndexMapping.combinationToIndex(isTopEdge, 4);

        // corners permutation
        int cornersPermutation =
            IndexMapping.permutationToIndex(state.cornersPermutation);

        // corners combination
        boolean[] isTopCorner = new boolean[8];
        for (int k = 0; k < isTopCorner.length; k++) {
            isTopCorner[k] = state.cornersPermutation[k] < 4;
        }
        int cornersCombination = IndexMapping.combinationToIndex(isTopCorner, 4);

        for (int depth = 0; ; depth++) {
            ArrayList<Integer> solution = new ArrayList<Integer>();
            if (search2(
                edgesPermutation,
                edgesCombination,
                cornersPermutation,
                cornersCombination,
                depth,
                solution,
                -1)) {
                String[] sequence = new String[solution.size()];
                for (int i = 0; i < solution.size(); i++) {
                    sequence[i] = moveNames2[solution.get(i)];
                }

                return sequence;
            }
        }
    }

    private boolean search2(int ep, int ec, int co, int cc, int depth, ArrayList<Integer> sol, int lf) {
        if (depth == 0) return ep == 0 && co == 0;

        if (epd[ep][cc] <= depth &&
            cpd[co][ec] <= depth) {
            for (int i = 0; i < 8; i++) {
                if (i == lf) {
                    continue;
                }
                int y = ep, s = ec;
                int t = co, u = cc;
                for(int k = 0; k < faces2[i]; k++){
                	y = epm[y][i]; s = ecm[s][i];
                	t = cpm[t][i]; u = ccm[u][i];
                	if(search2(y, s, t, u, depth - 1, sol, i)){
                		sol.add(index2[i]+k);
                		return true;
                	}
                }
            }
        }

        return false;
    }

    public String[] generate(State state) {
        String[] solution = solve(state);

        HashMap<String, String> inverseMoveNames = new HashMap<String, String>();
        inverseMoveNames.put("Uw",  "Uw'");
        inverseMoveNames.put("Uw2", "Uw2");
        inverseMoveNames.put("Uw'", "Uw");
        inverseMoveNames.put("Dw",  "Dw'");
        inverseMoveNames.put("Dw2", "Dw2");
        inverseMoveNames.put("Dw'", "Dw");
        inverseMoveNames.put("Lw",  "Lw'");
        inverseMoveNames.put("Lw2", "Lw2");
        inverseMoveNames.put("Lw'", "Lw");
        inverseMoveNames.put("Rw",  "Rw'");
        inverseMoveNames.put("Rw2", "Rw2");
        inverseMoveNames.put("Rw'", "Rw");
        inverseMoveNames.put("Fw",  "Fw'");
        inverseMoveNames.put("Fw2", "Fw2");
        inverseMoveNames.put("Fw'", "Fw");
        inverseMoveNames.put("Bw",  "Bw'");
        inverseMoveNames.put("Bw2", "Bw2");
        inverseMoveNames.put("Bw'", "Bw");
        inverseMoveNames.put("U",  "U'");
        inverseMoveNames.put("U2", "U2");
        inverseMoveNames.put("U'", "U");
        inverseMoveNames.put("D",  "D'");
        inverseMoveNames.put("D2", "D2");
        inverseMoveNames.put("D'", "D");

        String[] sequence = new String[solution.length];
        for (int i = 0; i < sequence.length; i++) {
            sequence[i] = inverseMoveNames.get(solution[solution.length - 1 - i]);
        }

        return sequence;
    }

    public State getRandomState(Random random) {
        byte[] orientation =
            IndexMapping.indexToZeroSumOrientation(
                random.nextInt(2187), 3, 8);
        byte[] edgesPermutation =
            IndexMapping.indexToPermutation(
                random.nextInt(40320), 8);
        byte[] cornersPermutation =
            IndexMapping.indexToPermutation(
                random.nextInt(40320), 8);

        return new State(orientation, edgesPermutation, cornersPermutation);
    }
}
