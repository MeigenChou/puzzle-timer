package com.puzzletimer.solvers;

import java.util.HashMap;
import java.util.Random;

import com.puzzletimer.solvers.RubiksCubeSolver.State;

public class RubiksCubeRUSolver {
    // moves
    private static String[] moveNames = {
        "U", "U2", "U'",
        "R", "R2", "R'",
    };
    private static State[] moves = new State[2];

    static {
        for (int i = 0; i < 2; i++) {
            moves[i] = RubiksCubeSolver.State.moves.get(moveNames[i*3]);
        }
    }

    // constants
    private static int N_CORNERS_PERMUTATIONS = 720;
    private static int N_CORNERS_ORIENTATIONS = 729;
    private static int N_EDGES_PERMUTATIONS = 5040;

    private static int goalCornersPermutation;
    private static int goalCornersOrientation;
    private static int goalEdgesPermutation;

    static {
        int[] goalIndices =
            stateToIndices(RubiksCubeSolver.State.id);

        goalCornersPermutation = goalIndices[0];
        goalCornersOrientation = goalIndices[1];
        goalEdgesPermutation = goalIndices[2];
    }

    // move tables
    private static short[][] cpm;
    private static short[][] com;
    private static short[][] epm;

    static {
        // corners permutation
        cpm = new short[N_CORNERS_PERMUTATIONS][moves.length];
        for (int i = 0; i < N_CORNERS_PERMUTATIONS; i++) {
            State state = indicesToState(new int[] { i, 0, 0, 0 });
            for (int j = 0; j < moves.length; j++) {
                int[] indices = stateToIndices(state.multiply(moves[j]));
                cpm[i][j] = (short) indices[0];
            }
        }

        // corners orientation
        com = new short[N_CORNERS_ORIENTATIONS][moves.length];
        for (int i = 0; i < N_CORNERS_ORIENTATIONS; i++) {
            State state = indicesToState(new int[] { 0, i, 0, 0 });
            for (int j = 0; j < moves.length; j++) {
                int[] indices = stateToIndices(state.multiply(moves[j]));
                com[i][j] = (short) indices[1];
            }
        }

        // edges permutation
        epm = new short[N_EDGES_PERMUTATIONS][moves.length];
        for (int i = 0; i < N_EDGES_PERMUTATIONS; i++) {
            State state = indicesToState(new int[] { 0, 0, i, 0 });
            for (int j = 0; j < moves.length; j++) {
                int[] indices = stateToIndices(state.multiply(moves[j]));
                epm[i][j] = (short) indices[2];
            }
        }
    }

    private static int[] stateToIndices(State state) {
        // corners
        boolean[] selectedCorners = {
            true,  true,  true,  true,
            false, true,  true,  false,
        };

        byte[] cornersMapping = {
             0,  1,  2,  3,
            -1,  5,  6, -1,
        };

        byte[] cornersPermutation = new byte[6];
        byte[] cornersOrientation = new byte[6];
        int next = 0;
        for (int i = 0; i < state.cornersPermutation.length; i++) {
            if (selectedCorners[i]) {
                cornersPermutation[next] = cornersMapping[state.cornersPermutation[i]];
                cornersOrientation[next] = state.cornersOrientation[i];
                next++;
            }
        }
        int cornersPermutationIndex =
            IndexMapping.permutationToIndex(cornersPermutation);
        int cornersOrientationIndex =
            IndexMapping.orientationToIndex(cornersOrientation, 3);

        // edges
        boolean[] selectedEdges = {
            false, true,  true,  false,
            true,  true,  true,  true,
            false, true,  false, false,
        };

        byte[] edgesMapping = {
            -1,  0,  1, -1,
             2,  3,  4,  5,
            -1,  6, -1, -1,
        };

        byte[] edgesPermutation = new byte[7];
        byte[] edgesOrientation = new byte[7];
        next = 0;
        for (int i = 0; i < state.edgesPermutation.length; i++) {
            if (selectedEdges[i]) {
                edgesPermutation[next] = edgesMapping[state.edgesPermutation[i]];
                edgesOrientation[next] = state.edgesOrientation[i];
                next++;
            }
        }
        int edgesPermutationIndex =
            IndexMapping.permutationToIndex(edgesPermutation);
        int edgesOrientationIndex =
            IndexMapping.orientationToIndex(edgesOrientation, 2);

        return new int[] {
            cornersPermutationIndex,
            cornersOrientationIndex,
            edgesPermutationIndex,
            edgesOrientationIndex,
        };
    }

    private static State indicesToState(int[] indices) {
        // corners
        boolean[] combination = {
            true,  true,  true,  true,
            false, true,  true,  false,
        };
        byte[] permutation =
            IndexMapping.indexToPermutation(indices[0], 6);
        byte[] orientation =
            IndexMapping.indexToOrientation(indices[1], 3, 6);

        byte[] selectedCorners = { 0, 1, 2, 3, 5, 6 };
        int nextSelectedCornerIndex = 0;
        byte[] otherCorners = { 4, 7 };
        int nextOtherCornerIndex = 0;

        byte[] cornersPermutation = new byte[8];
        byte[] cornersOrientation = new byte[8];
        for (int i = 0; i < cornersPermutation.length; i++) {
            if (combination[i]) {
                cornersPermutation[i] = selectedCorners[permutation[nextSelectedCornerIndex]];
                cornersOrientation[i] = orientation[nextSelectedCornerIndex];
                nextSelectedCornerIndex++;
            } else {
                cornersPermutation[i] = otherCorners[nextOtherCornerIndex];
                cornersOrientation[i] = 0;
                nextOtherCornerIndex++;
            }
        }

        // edges
        combination = new boolean[] {
            false, true,  true,  false,
            true,  true,  true,  true,
            false, true,  false, false,
        };
        permutation =
            IndexMapping.indexToPermutation(indices[2], 7);
        
        byte[] selectedEdges = { 1, 2, 4, 5, 6, 7, 9 };
        int nextSelectedEdgeIndex = 0;
        byte[] otherEdges = { 0, 3, 8, 10, 11 };
        int nextOtherEdgeIndex = 0;

        byte[] edgesPermutation = new byte[12];
        for (int i = 0; i < edgesPermutation.length; i++) {
            if (combination[i]) {
                edgesPermutation[i] = selectedEdges[permutation[nextSelectedEdgeIndex]];
                nextSelectedEdgeIndex++;
            } else {
                edgesPermutation[i] = otherEdges[nextOtherEdgeIndex];
                nextOtherEdgeIndex++;
            }
        }

        return new State(
            cornersPermutation,
            cornersOrientation,
            edgesPermutation,
            new byte[12]);
    }

    // distance tables
    private static byte[][] cornersDistance;
    private static byte[] edgesDistance;


    static {
        // corners
        cornersDistance = new byte[N_CORNERS_PERMUTATIONS][N_CORNERS_ORIENTATIONS];
        for (int i = 0; i < cornersDistance.length; i++) {
            for (int j = 0; j < cornersDistance[i].length; j++) {
                cornersDistance[i][j] = -1;
            }
        }
        cornersDistance[goalCornersPermutation][goalCornersOrientation] = 0;

        int distance = 0;
        int nVisited;
        do {
            nVisited = 0;

            for (int i = 0; i < cornersDistance.length; i++) {
                for (int j = 0; j < cornersDistance[i].length; j++) {
                    if (cornersDistance[i][j] != distance) {
                        continue;
                    }

                    for (int k = 0; k < cpm[i].length; k++) {
                    	int y = i, s = j;
                    	for(int m = 0; m < 3; m++) {
                    		y = cpm[y][k];
                    		s = com[s][k];
                    		if (cornersDistance[y][s] < 0) {
                                cornersDistance[y][s] =
                                    (byte) (distance + 1);
                                nVisited++;
                            }
                    	}
                    }
                }
            }

            distance++;
        } while (nVisited > 0);

        // edges
        edgesDistance = new byte[N_EDGES_PERMUTATIONS];
        for (int i = 0; i < edgesDistance.length; i++) {
                edgesDistance[i] = -1;
        }
        edgesDistance[goalEdgesPermutation] = 0;

        distance = 0;
        do {
            nVisited = 0;

            for (int i = 0; i < edgesDistance.length; i++) {
                    if (edgesDistance[i] != distance) {
                        continue;
                    }

                    for (int k = 0; k < epm[i].length; k++) {
                    	int y = i;
                    	for(int m = 0; m < 3; m++) {
                    		y = epm[y][k];
                    		if (edgesDistance[y] < 0) {
                                edgesDistance[y] = (byte) (distance + 1);
                                nVisited++;
                            }
                    	}
                    }
            }

            distance++;
        } while (nVisited > 0);

    }

    public static String[] solve(State state) {
        int[] indices = stateToIndices(state);

        for (int depth = 0; ; depth++) {
            int[] solution = new int[depth];

            if (search(
                    indices[0],
                    indices[1],
                    indices[2],
                    depth,
                    solution)) {
                String[] sequence = new String[solution.length];
                for (int i = 0; i < sequence.length; i++) {
                    sequence[i] = moveNames[solution[i]];
                }

                return sequence;
            }
        }
    }

    private static boolean search(int cp, int co, int ep, int depth, int[] sol) {
        if (depth == 0) {
            return cp == goalCornersPermutation &&
                   co == goalCornersOrientation &&
                   ep == goalEdgesPermutation;
        }

        if (cornersDistance[cp][co] > depth ||
            edgesDistance[ep] > depth) {
            return false;
        }

        for (int i = 0; i < moves.length; i++) {
            if (sol.length - depth > 0) {
                if (sol[sol.length - depth - 1] / 3 == i) {
                    continue;
                }
            }
            int y = cp, s = co, t = ep;
            for(int j = 0; j < 3; j++){
            	y = cpm[y][i]; s = com[s][i]; t = epm[t][i];
            	sol[sol.length - depth] = i*3+j;
            	if (search(y, s, t, depth - 1, sol)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String[] generate(State state) {
        HashMap<String, String> inverseMoveNames = new HashMap<String, String>();
        inverseMoveNames.put("U",  "U'");
        inverseMoveNames.put("U2", "U2");
        inverseMoveNames.put("U'", "U");
        inverseMoveNames.put("R",  "R'");
        inverseMoveNames.put("R2", "R2");
        inverseMoveNames.put("R'", "R");

        String[] solution = solve(state);

        String[] sequence = new String[solution.length];
        for (int i = 0; i < solution.length; i++) {
            sequence[i] = inverseMoveNames.get(solution[solution.length - i - 1]);
        }

        return sequence;
    }

    public static State getRandomState(Random random) {
        for (;;) {
            int cornersPermutation = random.nextInt(N_CORNERS_PERMUTATIONS);
            int cornersOrientation = random.nextInt(N_CORNERS_ORIENTATIONS);
            int edgesPermutation = random.nextInt(N_EDGES_PERMUTATIONS);

            if (cornersDistance[cornersPermutation][cornersOrientation] < 0 ||
                edgesDistance[edgesPermutation] < 0) {
                continue;
            }

            State state = indicesToState(
                new int[] {
                    cornersPermutation,
                    cornersOrientation,
                    edgesPermutation,
                });

            if (permutationSign(state.cornersPermutation) ==
                permutationSign(state.edgesPermutation)) {
                return state;
            }
        }
    }

    private static int permutationSign(byte[] permutation) {
        int nInversions = 0;
        for (int i = 0; i < permutation.length; i++) {
            for (int j = i + 1; j < permutation.length; j++) {
                if (permutation[i] > permutation[j]) {
                    nInversions++;
                }
            }
        }

        return nInversions % 2 == 0 ? 1 : -1;
    }
}
