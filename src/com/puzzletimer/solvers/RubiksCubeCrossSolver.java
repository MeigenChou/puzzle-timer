package com.puzzletimer.solvers;

import java.util.ArrayList;

import com.puzzletimer.solvers.RubiksCubeSolver.State;

public class RubiksCubeCrossSolver {
    // moves
    private static State[] moves = new State[6];

    static {
    	String[] turn = {"U", "D", "L", "R", "F", "B"};
        for (int i = 0; i < 6; i++) {
            moves[i] = State.moves.get(turn[i]);
        }
    }

    // move tables
    protected static short[][] epm = new short[11880][6];
    protected static short[][] eom = new short[7920][6];
	// distance tables
    protected static byte[] epd = new byte[11880];
    protected static byte[] eod = new byte[7920];

    private static boolean ini = false;
    protected static void init() {
    	if(ini)return;
    	// edges permutation and orientation
    	for (int i = 0; i < 495; i++) {
            for (int j = 0; j < 24; j++) {
                State state = indicesToState(new int[] { i, j, j });
                for (int k = 0; k < 6; k++) {
                    int[] indices = stateToIndices(state.multiply(moves[k]));
                    epm[i * 24 + j][k] = (short) (indices[0] * 24 + indices[1]);
                    if(j < 16) eom[i * 16 + j][k] =
                            (short) (indices[0] * 16 + indices[2]);
                }
            }
        }
    	
        // edges permutation
        for (int i = 0; i < epd.length; i++) {
            epd[i] = -1;
        }
        epd[11856] = 0;

        int distance = 0;
        int nVisited = 1;
        while (nVisited < 11880) {
            for (int i = 0; i < epd.length; i++) {
                if (epd[i] == distance) {
                	for (int j = 0; j < 6; j++) {
                    	for (int y = i, m = 0; m < 3; m++){
                    		y = epm[y][j];
                    		if (epd[y] == -1) {
                    			epd[y] = (byte) (distance + 1);
        						nVisited++;
        					}
                    	}
                    }
                }
            }
            distance++;
        }

        // edges orientation
        for (int i = 0; i < eod.length; i++) {
            eod[i] = -1;
        }
        eod[7904] = 0;

        distance = 0;
        nVisited = 1;
        while (nVisited < 7920) {
            for (int i = 0; i < eod.length; i++) {
                if (eod[i] == distance) {
                	for (int j = 0; j < 6; j++) {
                    	for (int y = i, m = 0; m < 3; m++){
                    		y = eom[y][j];
                    		if (eod[y] == -1) {
                    			eod[y] = (byte) (distance+1);
                                nVisited++;
                            }
                    	}
                    }
                }
            }
            distance++;
        }
        ini = true;
    }

    protected static int[] stateToIndices(State state) {
        // edges
        boolean[] selectedEdges = {
            false, false, false, false,
            false, false, false, false,
            true,  true,  true,  true,
        };

        byte[] edgesMapping = {
           -1, -1, -1, -1,
           -1, -1, -1, -1,
            0,  1,  2,  3,
        };

        boolean[] edgesCombination = new boolean[12];
        for (int i = 0; i < 12; i++) {
            edgesCombination[i] = selectedEdges[state.edgesPermutation[i]];
        }
        int edgesCombinationIndex =
            IndexMapping.combinationToIndex(edgesCombination, 4);

        byte[] edgesPermutation = new byte[4];
        byte[] edgesOrientation = new byte[4];
        int next = 0;
        for (int i = 0; i < 12; i++) {
            if (edgesCombination[i]) {
                edgesPermutation[next] = edgesMapping[state.edgesPermutation[i]];
                edgesOrientation[next++] = state.edgesOrientation[i];
            }
        }
        int edgesPermutationIndex =
            IndexMapping.permutationToIndex(edgesPermutation);
        int edgesOrientationIndex =
            IndexMapping.orientationToIndex(edgesOrientation, 2);

        return new int[] {
            edgesCombinationIndex,
            edgesPermutationIndex,
            edgesOrientationIndex,
        };
    }

    private static State indicesToState(int[] indices) {
        boolean[] combination =
            IndexMapping.indexToCombination(indices[0], 4, 12);
        byte[] permutation =
            IndexMapping.indexToPermutation(indices[1], 4);
        byte[] orientation =
            IndexMapping.indexToOrientation(indices[2], 2, 4);

        byte[] selectedEdges = { 8, 9, 10, 11 };
        int nextSelectedEdgeIndex = 0;
        byte[] otherEdges = { 0, 1, 2, 3, 4, 5, 6, 7 };
        int nextOtherEdgeIndex = 0;

        byte[] edgesPermutation = new byte[12];
        byte[] edgesOrientation = new byte[12];
        for (int i = 0; i < edgesPermutation.length; i++) {
            if (combination[i]) {
                edgesPermutation[i] = selectedEdges[permutation[nextSelectedEdgeIndex]];
                edgesOrientation[i] = orientation[nextSelectedEdgeIndex];
                nextSelectedEdgeIndex++;
            } else {
                edgesPermutation[i] = otherEdges[nextOtherEdgeIndex];
                edgesOrientation[i] = 0;
                nextOtherEdgeIndex++;
            }
        }

        return new State(
            State.id.cornersPermutation,
            State.id.cornersOrientation,
            edgesPermutation,
            edgesOrientation);
    }

    public static ArrayList<String[]> solve(State state) {
    	init();

        int[] indices = stateToIndices(state);

        int epi = indices[0] * 24 + indices[1];
        int eoi = indices[0] * 16 + indices[2];

        ArrayList<String[]> solutions = new ArrayList<String[]>();

        for (int depth = 0; ; depth++) {
            int[] path = new int[depth];

            search(epi, eoi, depth, path,
                   solutions, -1);

            if (solutions.size() > 0) {
                return solutions;
            }
        }
    }

    private static String[] moveNames = {
        "U", "U2", "U'",
        "D", "D2", "D'",
        "L", "L2", "L'",
        "R", "R2", "R'",
        "F", "F2", "F'",
        "B", "B2", "B'",
    };

    private static void search(int ep, int eo, int d,
            int[] path, ArrayList<String[]> sol, int lf) {
        if (d == 0) {
            if (ep == 11856 && eo == 7904) {
                String[] sequence = new String[path.length];
                for (int i = 0; i < sequence.length; i++) {
                    sequence[i] = moveNames[path[i]];
                }
                sol.add(sequence);
            }
            return;
        }

        if (epd[ep] > d || eod[eo] > d) return;

        for (int i = 0; i < 6; i++) {
        	if(i != lf) {
        		int y = ep, s = eo;
        		for(int j = 0; j < 3; j++){
        			y = epm[y][i]; s = eom[s][i];
        			path[path.length - d] = i * 3 + j;
        			search(y, s, d - 1, path, sol, i);
        		}
        	}
        }
    }
}
