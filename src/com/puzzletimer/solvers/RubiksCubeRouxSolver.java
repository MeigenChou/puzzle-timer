package com.puzzletimer.solvers;

import java.util.ArrayList;

import com.puzzletimer.solvers.RubiksCubeSolver.State;

public class RubiksCubeRouxSolver {
	// moves
    private static State[] moves = new State[6];

    static {
    	String[] turn = {"U", "D", "L", "R", "F", "B"};
        for (int i = 0; i < 6; i++) {
            moves[i] = State.moves.get(turn[i]);
        }
    }

    // move tables
    private static short[][] epm = new short[1320][6];
	private static short[][] eom = new short[1760][6];
	private static byte[][] cpm = new byte[56][6];
	private static short[][] com = new short[252][6];
	// distance tables
	private static byte[][] ed = new byte[220][48];
	private static byte[][] cd = new byte[28][18];
	
	private static boolean ini = false;
    private static void init() {
    	if(ini)return;
    	int i,j;
    	// edges permutation and orientation
    	for(i=0; i<220; i++){
    		for(j=0; j<8; j++){
    			State state = indicesToState(new int[]{0, 0, 0, i, j, j});
    			for(int k=0; k<6; k++){
    				int[] ind = stateToIndices(state.multiply(moves[k]));
    				eom[i*8+j][k] = (short)(ind[3]*8+ind[5]);
    				if(j<6)epm[i*6+j][k] = (short) (ind[3]*6+ind[4]);
    			}
    		}
    	}
    	for(i=0; i<28; i++) {
    		for (j = 0; j < 9; j++) {
    			State state = indicesToState(new int[]{i, j, j, 0, 0, 0});
    			for (int k = 0; k < 6; k++) {
    				int[] ind = stateToIndices(state.multiply(moves[k]));
    				com[i*9+j][k]=(short) (ind[0]*9+ind[2]);
    				if(j < 2)cpm[i*2+j][k] = (byte) (ind[0]*2+ind[1]);
    			}
    		}
    	}
    	for(i=0; i<220; i++)
			for(j=0; j<48; j++)
				ed[i][j]=-1;
    	ed[168][0]=0;
    	for(int d=0; d<7; d++) {
			//c=0;
			for(i=0; i<220; i++)
				for(j=0; j<48; j++)
					if(ed[i][j] == d)
						for(int l=0; l<6; l++){
							int x=i, y=j;
							for(int m=0; m<3; m++){
								y=(epm[x*6+y/8][l]%6)*8+eom[x*8+y%8][l]%8;
								x=epm[x*6+y/8][l]/6;
								if(ed[x][y]<0){
									ed[x][y]=(byte) (d+1);
									//c++;
								}
							}
						}
			//System.out.println(d+" "+c);
		}
    	for(i=0; i<28; i++)
			for(j=0; j<18; j++)
				cd[i][j]=-1;
    	cd[25][0]=0;
    	for(int d=0; d<4; d++){
			//c=0;
			for(i=0; i<28; i++)
				for(j=0; j<18; j++)
					if(cd[i][j] == d)
						for(int l=0; l<6; l++){
							int x=i, y=j;
							for(int m=0; m<3; m++){
								y=(cpm[x*2+y/9][l]%2)*9+com[x*9+y%9][l]%9;
								x=cpm[x*2+y/9][l]/2;
								if(cd[x][y]<0){
									cd[x][y]=(byte) (d+1);
									//c++;
								}
							}
						}
			//System.out.println(d+" "+c);
		}
    	ini = true;
    }
    
    protected static int[] stateToIndices(State state) {
    	// corners
        boolean[] selectedCorners = {
            false, false, false, false,
            true,  false, false, true,
        };

        byte[] cornersMapping = {
           -1, -1, -1, -1,
            0, -1, -1,  1,
        };

        boolean[] cornersCombination = new boolean[8];
        for (int i = 0; i < 8; i++) {
            cornersCombination[i] = selectedCorners[state.cornersPermutation[i]];
        }
        int cornersCombinationIndex =
            IndexMapping.combinationToIndex(cornersCombination, 2);

        byte[] cornersPermutation = new byte[2];
        byte[] cornersOrientation = new byte[2];
        int next = 0;
        for (int i = 0; i < state.cornersPermutation.length; i++) {
            if (cornersCombination[i]) {
                cornersPermutation[next] = cornersMapping[state.cornersPermutation[i]];
                cornersOrientation[next++] = state.cornersOrientation[i];
            }
        }
        int cornersPermutationIndex =
            IndexMapping.permutationToIndex(cornersPermutation);
        int cornersOrientationIndex =
            IndexMapping.orientationToIndex(cornersOrientation, 3);

        // edges
        boolean[] selectedEdges = {
        	true,  false, false, true,
            false, false, false, false,
            false, false, false, true,
        };

        byte[] edgesMapping = {
            0, -1, -1,  1,
           -1, -1, -1, -1,
           -1, -1, -1,  2,
        };

        boolean[] edgesCombination = new boolean[12];
        for (int i = 0; i < 12; i++) {
            edgesCombination[i] = selectedEdges[state.edgesPermutation[i]];
        }
        int edgesCombinationIndex =
            IndexMapping.combinationToIndex(edgesCombination, 3);

        byte[] edgesPermutation = new byte[3];
        byte[] edgesOrientation = new byte[3];
        next = 0;
        for (int i = 0; i < 12; i++) {
            if (edgesCombination[i]) {
                edgesPermutation[next] = edgesMapping[state.edgesPermutation[i]];
                edgesOrientation[next++] = state.edgesOrientation[i];
            }
        }
        int edgesPermutationIndex =
            IndexMapping.permutationToIndex(edgesPermutation);
        int edgesOrientationIndex =
            IndexMapping.orientationToIndex(edgesOrientation, 2);

        return new int[] {
        	cornersCombinationIndex,
            cornersPermutationIndex,
            cornersOrientationIndex,
            edgesCombinationIndex,
            edgesPermutationIndex,
            edgesOrientationIndex,
        };
    }

    private static State indicesToState(int[] indices) {
    	// corners
        boolean[] combination =
            IndexMapping.indexToCombination(indices[0], 2, 8);
        byte[] permutation =
            IndexMapping.indexToPermutation(indices[1], 2);
        byte[] orientation =
            IndexMapping.indexToOrientation(indices[2], 3, 2);

        byte[] selectedCorners = { 4, 7 };
        int nextSelectedCornerIndex = 0;
        byte[] otherCorners = { 0, 1, 2, 3, 5, 6 };
        int nextOtherCornerIndex = 0;

        byte[] cornersPermutation = new byte[8];
        byte[] cornersOrientation = new byte[8];
        for (int i = 0; i < cornersPermutation.length; i++) {
            if (combination[i]) {
                cornersPermutation[i] = selectedCorners[permutation[nextSelectedCornerIndex]];
                cornersOrientation[i] = orientation[nextSelectedCornerIndex];
                nextSelectedCornerIndex++;
            } else {
                cornersPermutation[i] = otherCorners[nextOtherCornerIndex];
                cornersOrientation[i] = 0;
                nextOtherCornerIndex++;
            }
        }

    	// edges
        combination =
            IndexMapping.indexToCombination(indices[3], 3, 12);
        permutation =
            IndexMapping.indexToPermutation(indices[4], 3);
        orientation =
            IndexMapping.indexToOrientation(indices[5], 2, 3);

        byte[] selectedEdges = { 0, 3, 11 };
        int nextSelectedEdgeIndex = 0;
        byte[] otherEdges = { 1, 2, 4, 5, 6, 7, 8, 9, 10 };
        int nextOtherEdgeIndex = 0;

        byte[] edgesPermutation = new byte[12];
        byte[] edgesOrientation = new byte[12];
        for (int i = 0; i < edgesPermutation.length; i++) {
            if (combination[i]) {
                edgesPermutation[i] = selectedEdges[permutation[nextSelectedEdgeIndex]];
                edgesOrientation[i] = orientation[nextSelectedEdgeIndex];
                nextSelectedEdgeIndex++;
            } else {
                edgesPermutation[i] = otherEdges[nextOtherEdgeIndex];
                edgesOrientation[i] = 0;
                nextOtherEdgeIndex++;
            }
        }

        return new State(
            cornersPermutation,
            cornersOrientation,
            edgesPermutation,
            edgesOrientation);
    }
    
    private static String[] turn={"U","D","L","R","F","B"};
	private static String[] suff={"","2","'"};
	private static StringBuffer sb=new StringBuffer();
	
	private static String[] moveNames = {
        "U", "U2", "U'",
        "D", "D2", "D'",
        "L", "L2", "L'",
        "R", "R2", "R'",
        "F", "F2", "F'",
        "B", "B2", "B'",
    };
	private static void search2(int cp, int co, int ep, int eo, int d, int idx, 
			int[] path, ArrayList<String> sol, int lm) {
		if (d == 0) {
			if (cp==50 && co==225 && ep==1008 && eo==1344) {
				StringBuffer sb = new StringBuffer(rotIdx2[idx]);
				for (int i = 0; i < path.length; i++) {
                    sb.append(" "+moveNames[path[i]]);
                }
				sol.add(sb.toString());
			}
			return;
		}
		if (ed[ep/6][(ep%6)*8+eo%8] > d || cd[cp/2][(cp%2)*9+co%9] > d) return;
		for (int i = 0; i < 6; i++) {
			if(i!=lm) {
				for(int u=cp, w=co, y=ep, s=eo, j=0;j<3;j++){
					u=cpm[u][i];w=com[w][i];
					y=epm[y][i];s=eom[s][i];
					path[path.length - d] = i * 3 + j;
					search2(u, w, y, s, d-1, idx, path, sol, i);
				}
			}
		}
	}
	private static boolean search(int cp, int co, int ep, int eo, int depth, int lm) {
		if (depth == 0) return (cp==50 && co==225 && ep==1008 && eo==1344);
		if (ed[ep/6][(ep%6)*8+eo%8] > depth || cd[cp/2][(cp%2)*9+co%9] > depth) return false;
		//if(pd[ep][cp]>depth || od[eo][co]>depth)return false;
		for (int i = 0; i < 6; i++)
			if(i!=lm)
				for(int d=cp, w=co, y=ep, s=eo, j=0;j<3;j++){
					d=cpm[d][i];w=com[w][i];
					y=epm[y][i];s=eom[s][i];
					if(search(d, w, y, s, depth-1, i)){
						sb.insert(0, " "+turn[i]+suff[j]);
						return true;
					}
				}
		return false;
	}
	private static String[][] moveIdx={
		{"LRDUFB","RLDUBF","BFDULR","FBDURL"},
		{"RLUDFB","LRUDBF","BFUDRL","FBUDLR"},
		{"UDLRFB","DULRBF","BFLRUD","FBLRDU"},
		{"DURLFB","UDRLBF","BFRLDU","FBRLUD"},
		{"UDFBRL","DUFBLR","LRFBUD","RLFBDU"},
		{"UDBFLR","DUBFRL","RLBFUD","LRBFDU"}};
	//private static String[] moveIdx={"LRDUFB","RLUDFB","UDLRFB","DURLFB","UDFBRL","UDBFLR"};
	private static String[][] color={
		{"DL","DF","DR","DB"},{"UR","UF","UL","UB"},
		{"LU","LF","LD","LB"},{"RD","RF","RU","RB"},
		{"FU","FR","FD","FL"},{"BU","BL","BD","BR"}};
	private static String[] rotIdx={"z", "z'", "", "z2", "y", "y'"};
	private static String[] rotIdx2={"", " x2", " x'", " x"};
	private static int[] scp={50, 7, 49, 12}, sco={225, 27, 221, 61};
	private static int[] sep={1008, 230, 1139, 1120}, seo={1344, 304, 1512, 1488};
	private static int[][] oriIdx={{1,0,2,3},{2,3,0,1},{0,1,3,2},{3,2,1,0}};
	public static String solve(String[] scr, int face, int side){
		init();
		int[] cp=new int[4], co=new int[4], ep=new int[4], eo=new int[4], o=new int[4];
		for(int i=0; i<4; i++){
			cp[i]=scp[oriIdx[side][i]];
			co[i]=sco[oriIdx[side][i]];
			ep[i]=sep[oriIdx[side][i]];
			eo[i]=seo[oriIdx[side][i]];
		}
		for(int d=0;d<scr.length;d++){
			if(0!=scr[d].length()){
				for(int i=0; i<4; i++)o[i]=moveIdx[face][i].indexOf(scr[d].charAt(0));
				for(int idx=0; idx<4; idx++) {
					cp[idx]=cpm[cp[idx]][o[idx]];co[idx]=com[co[idx]][o[idx]];
					ep[idx]=epm[ep[idx]][o[idx]];eo[idx]=eom[eo[idx]][o[idx]];
				}
				if(1<scr[d].length())
					if(scr[d].charAt(1)=='2')
						for(int idx=0; idx<4; idx++) {
							cp[idx]=cpm[cp[idx]][o[idx]];co[idx]=com[co[idx]][o[idx]];
							ep[idx]=epm[ep[idx]][o[idx]];eo[idx]=eom[eo[idx]][o[idx]];
						}
					else
						for(int idx=0; idx<4; idx++) {
							cp[idx]=cpm[cpm[cp[idx]][o[idx]]][o[idx]];co[idx]=com[com[co[idx]][o[idx]]][o[idx]];
							eo[idx]=eom[eom[eo[idx]][o[idx]]][o[idx]];ep[idx]=epm[epm[ep[idx]][o[idx]]][o[idx]];
						}
			}
		}
		sb=new StringBuffer();
		for(int d=0; ; d++){
			//System.out.print(d+" ");
			for(int idx=0; idx<4; idx++)
				if(search(cp[idx], co[idx], ep[idx], eo[idx], d, -1))
					return color[face][side]+": "+rotIdx[face]+rotIdx2[idx]+sb;
		}
	}
	public static ArrayList<String> solve2(String[] scr, int face, int side){
		init();
		int[] cp=new int[4], co=new int[4], ep=new int[4], eo=new int[4], o=new int[4];
		for(int i=0; i<4; i++){
			cp[i]=scp[oriIdx[side][i]];
			co[i]=sco[oriIdx[side][i]];
			ep[i]=sep[oriIdx[side][i]];
			eo[i]=seo[oriIdx[side][i]];
		}
		for(int d=0;d<scr.length;d++){
			if(0!=scr[d].length()){
				for(int i=0; i<4; i++)o[i]=moveIdx[face][i].indexOf(scr[d].charAt(0));
				for(int idx=0; idx<4; idx++) {
					cp[idx]=cpm[cp[idx]][o[idx]];co[idx]=com[co[idx]][o[idx]];
					ep[idx]=epm[ep[idx]][o[idx]];eo[idx]=eom[eo[idx]][o[idx]];
				}
				if(1<scr[d].length())
					if(scr[d].charAt(1)=='2')
						for(int idx=0; idx<4; idx++) {
							cp[idx]=cpm[cp[idx]][o[idx]];co[idx]=com[co[idx]][o[idx]];
							ep[idx]=epm[ep[idx]][o[idx]];eo[idx]=eom[eo[idx]][o[idx]];
						}
					else
						for(int idx=0; idx<4; idx++) {
							cp[idx]=cpm[cpm[cp[idx]][o[idx]]][o[idx]];co[idx]=com[com[co[idx]][o[idx]]][o[idx]];
							eo[idx]=eom[eom[eo[idx]][o[idx]]][o[idx]];ep[idx]=epm[epm[ep[idx]][o[idx]]][o[idx]];
						}
			}
		}
		ArrayList<String> sol = new ArrayList<String>();
		
		for(int d=0; ; d++){
			int[] path = new int[d];
			for(int idx=0; idx<4; idx++)
				search2(cp[idx], co[idx], ep[idx], eo[idx], d, idx, path, sol, -1);
			if(sol.size()>0) return sol;
//			for(int idx=0; idx<4; idx++)
//				if(search(cp[idx], co[idx], ep[idx], eo[idx], d, -1))
//					return color[face][side]+": "+rotIdx[face]+rotIdx2[idx]+sb;
		}
	}
}
