package com.puzzletimer.solvers;

import java.util.ArrayList;

import com.puzzletimer.solvers.RubiksCubeSolver.State;

public class RubiksCubeXCrossSolver {
    // moves
    private static String[] moveNames = {
            "U", "U2", "U'",
            "D", "D2", "D'",
            "L", "L2", "L'",
            "R", "R2", "R'",
            "F", "F2", "F'",
            "B", "B2", "B'",
        };
    private static State[] moves = new State[6];

    static {
        for (int i = 0; i < 6; i++) {
            moves[i] = RubiksCubeSolver.State.moves.get(moveNames[i*3]);
        }
    }

    private static byte[][] com = new byte[24][6];
    private static int[][] epm = new int[95040][6];
    private static short[][] eom = new short[25344][6];

    private static byte[] epd = new byte[95040], eod = new byte[792 * 32];

    private static boolean ini = false;
    static void init() {
    	if(ini) return;
    	long tm=System.currentTimeMillis();
        // corners orientation
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 3; j++) {
                State state = indicesToState(new int[] { i, 0, j, 0, 0, 0 });
                for (int k = 0; k < 6; k++) {
                    int[] indices = stateToIndices(state.multiply(moves[k]));
                    com[i * 3 + j][k] = (byte) (indices[0] * 3 + indices[2]);
                }
            }
        }

        // edges permutation and orientation
        for (int i = 0; i < 792; i++) {
            for (int j = 0; j < 120; j++) {
                State state = indicesToState(new int[] { 0, 0, 0, i, j, j });
                for (int k = 0; k < 6; k++) {
                    int[] indices = stateToIndices(state.multiply(moves[k]));
                    epm[i * 120 + j][k] = indices[3] * 120 + indices[4];
                    if(j<32) eom[i * 32 + j][k] =
                            (short) (indices[3] * 32 + indices[5]);
                }
            }
        }
        
        // edges permutation
        for (int i = 0; i < 95040; i++) {
            epd[i] = -1;
        }
        epd[94080] = 0;

        int distance = 0;
        int nVisited = 1;
        while (nVisited < 95040) {
            for (int i = 0; i < 95040; i++) {
                if (epd[i] != distance) {
                    continue;
                }
                for (int j = 0; j < 6; j++) {
                	for (int y = i, m = 0; m < 3; m++) {
                		y = epm[y][j];
                		if (epd[y] < 0) {
                			epd[y] = (byte) (distance + 1);
                			nVisited++;
                		}
                	}
                }
            }
            distance++;
        }

        // edges orientation
        for (int i = 0; i < eod.length; i++) {
            eod[i] = -1;
        }
        eod[25088] = 0;

        distance = 0;
        nVisited = 1;
        while (nVisited < 25344) {
            for (int i = 0; i < eod.length; i++) {
                if (eod[i] != distance) {
                    continue;
                }
                for (int j = 0; j < 6; j++) {
                	for (int y = i, m = 0; m < 3; m++) {
                		y = eom[y][j];
                		if (eod[y] < 0) {
                            eod[y] = (byte) (distance + 1);
                            nVisited++;
                        }
                	}
                }
            }
            distance++;
        }
        
        System.out.println(System.currentTimeMillis()-tm);
        ini = true;
    }

    private static int[] stateToIndices(State state) {
        // corners
        boolean[] selectedCorners = {
            false, false, false, false,
            true,  false, false, false,
        };

        byte[] cornersMapping = {
           -1, -1, -1, -1,
            0, -1, -1, -1,
        };

        boolean[] cornersCombination = new boolean[state.cornersPermutation.length];
        for (int i = 0; i < cornersCombination.length; i++) {
            cornersCombination[i] = selectedCorners[state.cornersPermutation[i]];
        }
        int cornersCombinationIndex =
            IndexMapping.combinationToIndex(cornersCombination, 1);

        byte[] cornersPermutation = new byte[1];
        byte[] cornersOrientation = new byte[1];
        int next = 0;
        for (int i = 0; i < state.cornersPermutation.length; i++) {
            if (cornersCombination[i]) {
                cornersPermutation[next] = cornersMapping[state.cornersPermutation[i]];
                cornersOrientation[next] = state.cornersOrientation[i];
                next++;
            }
        }
        int cornersPermutationIndex =
            IndexMapping.permutationToIndex(cornersPermutation);
        int cornersOrientationIndex =
            IndexMapping.orientationToIndex(cornersOrientation, 3);

        // edges
        boolean[] selectedEdges = {
            true,  false, false, false,
            false, false, false, false,
            true,  true,  true,  true,
        };

        byte[] edgesMapping = {
            0, -1, -1, -1,
           -1, -1, -1, -1,
            1,  2,  3,  4,
        };

        boolean[] edgesCombination = new boolean[state.edgesPermutation.length];
        for (int i = 0; i < edgesCombination.length; i++) {
            edgesCombination[i] = selectedEdges[state.edgesPermutation[i]];
        }
        int edgesCombinationIndex =
            IndexMapping.combinationToIndex(edgesCombination, 5);

        byte[] edgesPermutation = new byte[5];
        byte[] edgesOrientation = new byte[5];
        next = 0;
        for (int i = 0; i < state.edgesPermutation.length; i++) {
            if (edgesCombination[i]) {
                edgesPermutation[next] = edgesMapping[state.edgesPermutation[i]];
                edgesOrientation[next] = state.edgesOrientation[i];
                next++;
            }
        }
        int edgesPermutationIndex =
            IndexMapping.permutationToIndex(edgesPermutation);
        int edgesOrientationIndex =
            IndexMapping.orientationToIndex(edgesOrientation, 2);

        return new int[] {
            cornersCombinationIndex,
            cornersPermutationIndex,
            cornersOrientationIndex,
            edgesCombinationIndex,
            edgesPermutationIndex,
            edgesOrientationIndex,
        };
    }

    private static State indicesToState(int[] indices) {
        // corners
        boolean[] combination =
            IndexMapping.indexToCombination(indices[0], 1, 8);
        byte[] permutation =
            IndexMapping.indexToPermutation(indices[1], 1);
        byte[] orientation =
            IndexMapping.indexToOrientation(indices[2], 3, 1);

        byte[] selectedCorners = { 4 };
        int nextSelectedCornerIndex = 0;
        byte[] otherCorners = { 0, 1, 2, 3, 5, 6, 7 };
        int nextOtherCornerIndex = 0;

        byte[] cornersPermutation = new byte[8];
        byte[] cornersOrientation = new byte[8];
        for (int i = 0; i < cornersPermutation.length; i++) {
            if (combination[i]) {
                cornersPermutation[i] = selectedCorners[permutation[nextSelectedCornerIndex]];
                cornersOrientation[i] = orientation[nextSelectedCornerIndex];
                nextSelectedCornerIndex++;
            } else {
                cornersPermutation[i] = otherCorners[nextOtherCornerIndex];
                cornersOrientation[i] = 0;
                nextOtherCornerIndex++;
            }
        }

        // edges
        combination =
            IndexMapping.indexToCombination(indices[3], 5, 12);
        permutation =
            IndexMapping.indexToPermutation(indices[4], 5);
        orientation =
            IndexMapping.indexToOrientation(indices[5], 2, 5);

        byte[] selectedEdges = { 0, 8, 9, 10, 11 };
        int nextSelectedEdgeIndex = 0;
        byte[] otherEdges = { 1, 2, 3, 4, 5, 6, 7 };
        int nextOtherEdgeIndex = 0;

        byte[] edgesPermutation = new byte[12];
        byte[] edgesOrientation = new byte[12];
        for (int i = 0; i < edgesPermutation.length; i++) {
            if (combination[i]) {
                edgesPermutation[i] = selectedEdges[permutation[nextSelectedEdgeIndex]];
                edgesOrientation[i] = orientation[nextSelectedEdgeIndex];
                nextSelectedEdgeIndex++;
            } else {
                edgesPermutation[i] = otherEdges[nextOtherEdgeIndex];
                edgesOrientation[i] = 0;
                nextOtherEdgeIndex++;
            }
        }

        return new State(
            cornersPermutation,
            cornersOrientation,
            edgesPermutation,
            edgesOrientation);
    }

    public static ArrayList<String[]> solve(State state) {
    	init();
    	
        int[] indices = stateToIndices(state);

        int coi = indices[0] * 3 + indices[2];
        int epi = indices[3] * 120 + indices[4];
        int eoi = indices[3] * 32 + indices[5];

        ArrayList<String[]> sol = new ArrayList<String[]>();

        for (int depth = 0; ; depth++) {
            int[] path = new int[depth];

            search(coi, epi, eoi, depth, path, sol, -1);

            if (sol.size() > 0)
                return sol;
        }
    }

    private static void search(int co, int ep, int eo, int depth,
    		int[] path, ArrayList<String[]> sol, int l) {
        if (depth == 0) {
            if (co == 12 && ep == 94080 && eo == 25088) {
                String[] sequence = new String[path.length];
                for (int i = 0; i < sequence.length; i++) {
                    sequence[i] = moveNames[path[i]];
                }
                sol.add(sequence);
            }
            return;
        }

        if (epd[ep] > depth || eod[eo] > depth) return;

        for (int i = 0; i < 6; i++) {
        	if (i != l) {
        		int w = co, y = ep, s = eo;
        		for (int j = 0; j < 3; j++) {
        			w = com[w][i]; y = epm[y][i]; s = eom[s][i];
					path[path.length - depth] = i*3+j;
					search(w, y, s, depth - 1, path, sol, i);
        		}
        	}
        }
    }
}
