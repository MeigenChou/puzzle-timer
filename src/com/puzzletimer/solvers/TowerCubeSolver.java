package com.puzzletimer.solvers;

import java.util.ArrayList;
import java.util.Random;

public class TowerCubeSolver {
    protected static char[][] cpm = new char[40320][6];
    private static byte[][] epm = new byte[24][6];
    protected static byte[] cpd = new byte[40320];
    private static byte[] epd = new byte[24];
    private static byte[] faces = {3, 3, 1, 1, 1, 1};
    private static String[] turn = {"U", "D", "L", "R", "F", "B"};
    private static String[] suff={"'", "2", ""};
    private static int[] fact={1, 1, 2, 6, 24, 120, 720, 5040};

    static void set8Perm(int[] arr, int idx) {
		int val = 0x76543210;
		for (int i=0; i<7; i++) {
			int p = fact[7-i];
			int v = idx / p;
			idx -= v*p;
			v <<= 2;
			arr[i] = (val >> v) & 07;
			int m = (1 << v) - 1;
			val = (val & m) + ((val >> 4) & ~m);
		}
		arr[7] = val;
	}

	static int get8Perm(int[] arr) {
		int idx = 0;
		int val = 0x76543210;
		for (int i=0; i<7; i++) {
			int v = arr[i] << 2;
			idx = (8 - i) * idx + ((val >> v) & 07);
			val -= 0x11111110 << v;
		}
		return idx;
	}
	
	public static void cir(int[] arr, int a, int b, int c, int d){
    	int temp=arr[a]; arr[a]=arr[b]; arr[b]=arr[c]; arr[c]=arr[d]; arr[d]=temp;
    }
	
	public static void cir(byte[] arr, int a, int b){
		byte temp=arr[a]; arr[a]=arr[b]; arr[b]=temp;
    }
	
	public static void cir2(int[] arr, int a, int b, int c, int d){
    	int temp=arr[a]; arr[a]=arr[b]; arr[b]=temp;
    	temp=arr[c]; arr[c]=arr[d]; arr[d]=temp;
    }
	
    static {
    	int[] arr = new int[8];
    	for (int i = 0; i < 40320; i++) {
    		for (int j = 0; j < 6; j++) {
    			set8Perm(arr, i);
    			switch(j){
    			case 0:cir(arr, 0, 3, 2, 1);break;	//U
    			case 1:cir(arr, 4, 5, 6, 7);break;	//D
    			case 2:cir2(arr, 0, 7, 3, 4);break;	//L
    			case 3:cir2(arr, 1, 6, 2, 5);break;	//R
    			case 4:cir2(arr, 3, 6, 2, 7);break;	//F
    			case 5:cir2(arr, 0, 5, 1, 4);break;	//B
    			}
    			cpm[i][j]=(char) get8Perm(arr);
    		}
    	}
    	
    	for (int i = 1; i < 40320; i++)
    		cpd[i]=-1;
    	cpd[0]=0;
        for(int d=0; d<13; d++) {
        	for (int i = 0; i < 40320; i++)
        		if (cpd[i] == d)
        			for (int k = 0; k < 6; k++)
        				for(int y = i, m = 0; m < faces[k]; m++) {
        					y = cpm[y][k];
        					if (cpd[y] < 0) {
        						cpd[y] = (byte) (d + 1);
        					}
        				}
        }
        
        for(int i=0; i<24; i++) {
    		for (int j = 0; j < 6; j++) {
    			if(j<2)epm[i][j]=(byte) i;
    			else {
    				byte[] ar = IndexMapping.indexToPermutation(i, 4);
    				switch(j){
    				case 2:cir(ar, 0, 3);break;	//L
    				case 3:cir(ar, 1, 2);break;	//R
    				case 4:cir(ar, 3, 2);break;	//F
    				case 5:cir(ar, 1, 0);break;	//B
    				}
    				epm[i][j]=(byte) IndexMapping.permutationToIndex(ar);
    			}
    		}
    	}
    	
        for (int i = 1; i < 24; i++)
        	epd[i]=-1;
        epd[0]=0;
        for (int d = 0; d < 4; d++) {
        	for (int i = 0; i < 24; i++)
        		if (epd[i] == d)
        			for (int k = 2; k < 6; k++) {
        				byte next = epm[i][k];
        				if (epd[next] < 0) {
        					epd[next] = (byte) (d + 1);
        				}
        			}
        }
    }

    private static boolean search(int cp, int ep, int depth, ArrayList<String> sol, int l) {
    	if (depth == 0) return cp==0 && ep==0;
    	if (cpd[cp] > depth || epd[ep] > depth) return false;
    	int y, s;
    	for (int i = 0; i < 6; i++) {
    		if (i != l) {
    			y = cp; s = ep;
    			for(int k = 0; k < faces[i]; k++){
    				y = cpm[y][i]; s = epm[s][i];
    				if(search(y, s, depth - 1, sol, i)){
    					sol.add(turn[i] + (i<2?suff[k]:""));
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    public static String[] generate(Random r) {
        ArrayList<String> sequence = new ArrayList<String>();

        int cp=r.nextInt(40320);
        int ep=r.nextInt(24);
        
        for (int depth = 0;!search(cp, ep, depth, sequence, -1); depth++);

        String[] sequenceArray = new String[sequence.size()];
        sequence.toArray(sequenceArray);

        return sequenceArray;
    }
}
