package com.puzzletimer.solvers;

import java.util.ArrayList;
import java.util.Random;

public class SkewbSolver {
	private static short[][] fpm = new short[360][4];
	private static byte[][] cpm = new byte[12][4];
	private static byte[][] com = new byte[27][4];
	private static byte[][] fcm = new byte[81][4];
	private static byte[] fd = new byte[360];
	private static byte[][][] cd = new byte[12][27][81];
	
	private static void cir(byte[] arr, int a, int b, int c){
		byte temp = arr[a];
		arr[a] = arr[b];
		arr[b] = arr[c];
		arr[c] = temp;
	}
	
    private boolean initialized;

    public SkewbSolver() {
        this.initialized = false;
    }

    private void initialize() {
    	// move tables
		for (int i = 0; i < 360; i++)
			for (int j = 0; j < 4; j++) {
				byte[] arr = IndexMapping.indexToEvenPermutation(i, 6);
				switch(j){
				case 0: cir(arr, 0, 1, 4); break;
				case 1: cir(arr, 0, 3, 2); break;
				case 2: cir(arr, 3, 4, 5); break;
				case 3: cir(arr, 1, 2, 5); break;
				}
				fpm[i][j] = (short)IndexMapping.evenPermutationToIndex(arr);
			}
		
		for (int i = 0; i < 12; i++)
			for (int j = 0; j < 4; j++) {
				byte[] arr = IndexMapping.indexToEvenPermutation(i, 4);
				switch(j){
				case 0: cir(arr, 0, 2, 1); break;
				case 1: cir(arr, 0, 1, 3); break;
				case 2: cir(arr, 1, 2, 3); break;
				case 3: cir(arr, 0, 3, 2); break;
				}
				cpm[i][j] = (byte)IndexMapping.evenPermutationToIndex(arr);
			}

		for (int i = 0; i < 27; i++)
			for (int j = 0; j < 4; j++) {
				byte[] arr = IndexMapping.indexToZeroSumOrientation(i, 3, 4);
				switch(j){
				case 0: cir(arr, 0, 2, 1); arr[0] = (byte) ((arr[0] + 2) % 3);
				arr[1] = (byte) ((arr[1] + 2) % 3); arr[2] = (byte) ((arr[2] + 2) % 3); break;
				case 1: cir(arr, 0, 1, 3); arr[0] = (byte) ((arr[0] + 2) % 3);
				arr[1] = (byte) ((arr[1] + 2) % 3); arr[3] = (byte) ((arr[3] + 2) % 3); break;
				case 2: cir(arr, 1, 2, 3); arr[3] = (byte) ((arr[3] + 2) % 3);
				arr[1] = (byte) ((arr[1] + 2) % 3); arr[2] = (byte) ((arr[2] + 2) % 3); break;
				case 3: cir(arr, 0, 3, 2); arr[0] = (byte) ((arr[0] + 2) % 3);
				arr[3] = (byte) ((arr[3] + 2) % 3); arr[2] = (byte) ((arr[2] + 2) % 3); break;
				}
				com[i][j] = (byte)IndexMapping.zeroSumOrientationToIndex(arr, 3);
			}
		
		int[] ch = {0, 1, 3, 2};
		for (int i = 0; i < 81; i++)
			for (int j = 0; j < 4; j++) {
				byte[] arr = IndexMapping.indexToOrientation(i, 3, 4);
				arr[ch[j]] = (byte) ((arr[ch[j]] + 1) % 3);
				fcm[i][j] = (byte)IndexMapping.orientationToIndex(arr, 3);
			}
		
		// distance table
		for (int i = 0; i < 360; i++)
			fd[i]=-1;
		for (int j = 0; j < 12; j++)
			for(int k = 0; k < 27; k++)
				for(int l = 0; l < 81; l++)
					cd[j][k][l] = -1;
		fd[0] = 0; cd[0][0][0] = 0;
		for(int depth = 0; depth < 5; depth++) {
			//nVisited = 0;
			for (int i = 0; i < 360; i++)
				if (fd[i] == depth)
					for (int m = 0; m < 4; m++) {
						int p = i;
						for(int n = 0; n < 2; n++){
							p = fpm[p][m];
							if (fd[p] == -1) {
								fd[p] = (byte) (depth + 1);
								//nVisited++;
							}
						}
					}
			//System.out.println(depth+" "+nVisited);
		}
		
		for(int depth = 0; depth < 7; depth++) {
			//nVisited = 0;
			for (int j = 0; j < 12; j++)
				for (int k = 0; k < 27; k++)
					for (int l = 0; l< 81; l++)
						if (cd[j][k][l] == depth)
							for (int m = 0; m < 4; m++) {
								int p = j, q = k, r = l;
								for(int n = 0; n < 2; n++){
									p = cpm[p][m];
									q = com[q][m];
									r = fcm[r][m];
									if (cd[p][q][r] == -1) {
										cd[p][q][r] = (byte) (depth + 1);
										//nVisited++;
									}
								}
							}
			//System.out.println(depth+" "+nVisited);
		}
		
        this.initialized = true;
    }

    private static String[] turn = {"L", "R", "D", "B"};
	private static String[] suff = {"'", ""};
	private static boolean search(int fp, int cp, int co, int fco, int d, ArrayList<String> sol, int l) {
		if(d==0)return fd[fp] == 0 && cd[cp][co][fco] == 0;
		if(fd[fp] > d || cd[cp][co][fco] > d)return false;
		for(int k = 0; k < 4; k++)
			if(k != l){
				int p=fp, q=cp, r=co, s=fco;
				for(int m=0; m<2; m++){
					p=fpm[p][k]; q=cpm[q][k]; r=com[r][k]; s=fcm[s][k];
					if(search(p, q, r, s, d-1, sol, k)) {
						sol.add(turn[k]+suff[m]);
						return true;
					}
				}
			}
		return false;
	}
	
    public String[] generate(Random r) {
        if (!this.initialized) {
            initialize();
        }

        int fp = r.nextInt(360);
		int cp, co, fco;
		do{
			cp = r.nextInt(12);
			co = r.nextInt(27);
			fco = r.nextInt(81);
		}
		while (cd[cp][co][fco] < 0);

        ArrayList<String> sequence = new ArrayList<String>();

        for (int depth = 0;!search(fp, cp, co, fco, depth, sequence, -1); depth++);

        String[] sequenceArray = new String[sequence.size()];
        sequence.toArray(sequenceArray);

        return sequenceArray;
    }
}
