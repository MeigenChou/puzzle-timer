package com.puzzletimer.solvers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class RubiksPocketCubeSolver {
    public static class State {
        public byte[] permutation;
        public byte[] orientation;

        public State(byte[] permutation, byte[] orientation) {
            this.orientation = orientation;
            this.permutation = permutation;
        }

        public State multiply(State move) {
            byte[] resultPermutation = new byte[8];
            byte[] resultOrientation = new byte[8];

            for (int i = 0; i < 8; i++) {
                resultPermutation[i] = this.permutation[move.permutation[i]];
                resultOrientation[i] = (byte) ((this.orientation[move.permutation[i]] + move.orientation[i]) % 3);
            }

            return new State(resultPermutation, resultOrientation);
        }

        public static State id;

        static {
            id = new State(
                new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 },
                new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
        }
    }

    
    private int minScrambleLength;
    private ArrayList<State> moves;
    private ArrayList<String> moveNames;
    private boolean initialized;
    private char[][] pm;
    private short[][] om;
    private byte[] pd;
    private byte[] od;

    public RubiksPocketCubeSolver(int minScrambleLenght, String[] generatingSet) {
        this.minScrambleLength = minScrambleLenght;

        HashMap<String, State> table = new HashMap<String, State>();
        table.put("U", new State(new byte[] { 3, 0, 1, 2, 4, 5, 6, 7 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }));
        table.put("D", new State(new byte[] { 0, 1, 2, 3, 5, 6, 7, 4 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }));
        table.put("L", new State(new byte[] { 4, 1, 2, 0, 7, 5, 6, 3 }, new byte[] { 2, 0, 0, 1, 1, 0, 0, 2 }));
        table.put("R", new State(new byte[] { 0, 2, 6, 3, 4, 1, 5, 7 }, new byte[] { 0, 1, 2, 0, 0, 2, 1, 0 }));
        table.put("F", new State(new byte[] { 0, 1, 3, 7, 4, 5, 2, 6 }, new byte[] { 0, 0, 1, 2, 0, 0, 2, 1 }));
        table.put("B", new State(new byte[] { 1, 5, 2, 3, 0, 4, 6, 7 }, new byte[] { 1, 2, 0, 0, 2, 1, 0, 0 }));

        moves = new ArrayList<State>();
        moveNames = new ArrayList<String>();
        for (String moveName : generatingSet) {
            State move = table.get(moveName);

            moves.add(move);
            moveNames.add(moveName);

            //moves.add(move.multiply(move));
            moveNames.add(moveName + "2");

            //moves.add(move.multiply(move).multiply(move));
            moveNames.add(moveName + "'");
        }

        initialized = false;
    }

    private void initialize() {
    	long tm = System.currentTimeMillis();
        // move tables
        pm = new char[40320][moves.size()];
        for (int i = 0; i < 40320; i++) {
            State state = new State(IndexMapping.indexToPermutation(i, 8), new byte[8]);
            for (int j = 0; j < moves.size(); j++) {
                pm[i][j] = (char) IndexMapping.permutationToIndex(
                        state.multiply(moves.get(j)).permutation);
            }
        }

        om = new short[2187][moves.size()];
        for (int i = 0; i < 2187; i++) {
            State state = new State(new byte[8], IndexMapping.indexToZeroSumOrientation(i, 3, 8));
            for (int j = 0; j < moves.size(); j++) {
                om[i][j] = (short) IndexMapping.zeroSumOrientationToIndex(
                        state.multiply(moves.get(j)).orientation, 3);
            }
        }

        // prune tables
        pd = new byte[40320];
        for (int i = 0; i < 40320; i++)
            pd[i] = -1;
        pd[0] = 0;

        int nVisited;
        int depth = 0;
        do {
            nVisited = 0;
            for (int i = 0; i < 40320; i++) {
                if (pd[i] == depth) {
                    for (int j = 0; j < moves.size(); j++) {
                    	for(int y = i, k = 0; k < 3; k++) {
                    		y = pm[y][j];
                    		if (pd[y] < 0) {
                    			pd[y] = (byte) (depth + 1);
                    			nVisited++;
                    		}
                    	}
                    }
                }
            }
            depth++;
        } while (nVisited > 0);

        od = new byte[2187];
        for (int i = 0; i < od.length; i++) {
            od[i] = -1;
        }
        od[0] = 0;

        depth = 0;
        do {
            nVisited = 0;
            for (int i = 0; i < od.length; i++) {
                if (od[i] == depth) {
                    for (int j = 0; j < moves.size(); j++) {
                    	for(int y = i, k = 0; k < 3; k++) {
                    		y = om[y][j];
                    		if (od[y] < 0) {
                    			od[y] = (byte) (depth + 1);
                    			nVisited++;
                    		}
                    	}
                    }
                }
            }
            depth++;
        } while (nVisited > 0);
        System.out.println(System.currentTimeMillis()-tm);
        initialized = true;
    }

    public String[] solve(State state) {
        if (!initialized) {
            initialize();
        }

        int permutation =
            IndexMapping.permutationToIndex(state.permutation);
        int orientation =
            IndexMapping.zeroSumOrientationToIndex(state.orientation, 3);

        for (int depth = minScrambleLength;; depth++) {
            ArrayList<String> solution = new ArrayList<String>();
            if (search(permutation, orientation, depth, solution, -1)) {
                String[] sequence = new String[solution.size()];
                solution.toArray(sequence);

                return sequence;
            }
        }
    }

    private boolean search(int perm, int ori, int depth, ArrayList<String> sol, int lastFace) {
        if (depth == 0) return perm == 0 && ori == 0;

        if (pd[perm] <= depth && od[ori] <= depth) {
            for (int i = 0; i < moves.size(); i++) {
                if (i == lastFace) continue;
                int w = perm, y = ori;
                for (int j = 0; j < 3; j++) {
                	w = pm[w][i]; y = om[y][i];
                	if (search(w, y, depth - 1, sol, i)) {
                		sol.add(moveNames.get(i*3+j));
                		return true;
                	}
                }
            }
        }

        return false;
    }

    public String[] generate(State state) {
        HashMap<String, String> inverseMoveNames = new HashMap<String, String>();
        inverseMoveNames.put("U",  "U'");
        inverseMoveNames.put("U2", "U2");
        inverseMoveNames.put("U'", "U");
        inverseMoveNames.put("D",  "D'");
        inverseMoveNames.put("D2", "D2");
        inverseMoveNames.put("D'", "D");
        inverseMoveNames.put("L",  "L'");
        inverseMoveNames.put("L2", "L2");
        inverseMoveNames.put("L'", "L");
        inverseMoveNames.put("R",  "R'");
        inverseMoveNames.put("R2", "R2");
        inverseMoveNames.put("R'", "R");
        inverseMoveNames.put("F",  "F'");
        inverseMoveNames.put("F2", "F2");
        inverseMoveNames.put("F'", "F");
        inverseMoveNames.put("B",  "B'");
        inverseMoveNames.put("B2", "B2");
        inverseMoveNames.put("B'", "B");

        String[] solution = solve(state);

        String[] sequence = new String[solution.length];
        for (int i = 0; i < sequence.length; i++) {
            sequence[i] = inverseMoveNames.get(solution[solution.length - 1 - i]);
        }

        return sequence;
    }

    public State getRandomState(Random random) {
        State state = State.id;
        for (int i = 0; i < 100; i++) {
            state = state.multiply(moves.get(random.nextInt(moves.size())));
        }

        return state;
    }
}
