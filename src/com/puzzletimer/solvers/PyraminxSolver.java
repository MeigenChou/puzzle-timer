package com.puzzletimer.solvers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class PyraminxSolver {
    public static class State {
        public byte[] tipsOrientation;
        public byte[] verticesOrientation;
        public byte[] edgesPermutation;
        public byte[] edgesOrientation;

        public State(
                byte[] tipsOrientation,
                byte[] verticesOrientation,
                byte[] edgesPermutation,
                byte[] edgesOrientation) {
            this.tipsOrientation = tipsOrientation;
            this.verticesOrientation = verticesOrientation;
            this.edgesPermutation = edgesPermutation;
            this.edgesOrientation = edgesOrientation;
        }

        public State multiply(State move) {
            byte[] tipsOrientation = new byte[4];
            for (int i = 0; i < 4; i++) {
                tipsOrientation[i] = (byte) ((this.tipsOrientation[i] + move.tipsOrientation[i]) % 3);
            }

            byte[] verticesOrientation = new byte[4];
            for (int i = 0; i < 4; i++) {
                verticesOrientation[i] = (byte) ((this.verticesOrientation[i] + move.verticesOrientation[i]) % 3);
            }

            byte[] edgesPermutation = new byte[6];
            byte[] edgesOrientation = new byte[6];
            for (int i = 0; i < 6; i++) {
                edgesPermutation[i] = this.edgesPermutation[move.edgesPermutation[i]];
                edgesOrientation[i] = (byte) ((this.edgesOrientation[move.edgesPermutation[i]] + move.edgesOrientation[i]) % 2);
            }

            return new State(tipsOrientation, verticesOrientation, edgesPermutation, edgesOrientation);
        }
    }

    public final int N_TIPS_ORIENTATIONS = 81;
    public final int N_VERTICES_ORIENTATIONS = 81;
    public final int N_EDGES_PERMUTATIONS = 360;
    public final int N_EDGES_ORIENTATIONS = 32;

    private int minScrambleLength;
    private boolean initialized;
    private State[] tipMoves;
    private String[] tipMoveNames = {
    		"u", "u'",
    		"l", "l'",
    		"r", "r'",
    		"b", "b'",
    };
    private State[] moves;
    private String[] moveNames = {
    		"U", "U'",
    		"L", "L'",
    		"R", "R'",
    		"B", "B'",
    };
    private byte[][] tipsOm;
    private byte[][] verticesOm;
    private short[][] edgesPm;
    private byte[][] edgesOm;
    private byte[] tipsOd;
    private byte[] verticesOd;
    private byte[] edgesPd;
    private byte[] edgesOd;

    public PyraminxSolver(int minScrambleLength) {
        this.minScrambleLength = minScrambleLength;
        this.initialized = false;
    }

    private void initialize() {
        State moveu = new State(new byte[] { 1, 0, 0, 0 }, new byte[] { 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5 }, new byte[] { 0, 0, 0, 0, 0, 0 });
        State movel = new State(new byte[] { 0, 1, 0, 0 }, new byte[] { 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5 }, new byte[] { 0, 0, 0, 0, 0, 0 });
        State mover = new State(new byte[] { 0, 0, 1, 0 }, new byte[] { 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5 }, new byte[] { 0, 0, 0, 0, 0, 0 });
        State moveb = new State(new byte[] { 0, 0, 0, 1 }, new byte[] { 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5 }, new byte[] { 0, 0, 0, 0, 0, 0 });

        tipMoves = new State[] {moveu, movel, mover, moveb};

        State moveU = new State(new byte[] { 1, 0, 0, 0 }, new byte[] { 1, 0, 0, 0 }, new byte[] { 2, 0, 1, 3, 4, 5 }, new byte[] { 0, 0, 0, 0, 0, 0 });
        State moveL = new State(new byte[] { 0, 1, 0, 0 }, new byte[] { 0, 1, 0, 0 }, new byte[] { 0, 1, 5, 3, 2, 4 }, new byte[] { 0, 0, 1, 0, 0, 1 });
        State moveR = new State(new byte[] { 0, 0, 1, 0 }, new byte[] { 0, 0, 1, 0 }, new byte[] { 0, 4, 2, 1, 3, 5 }, new byte[] { 0, 1, 0, 0, 1, 0 });
        State moveB = new State(new byte[] { 0, 0, 0, 1 }, new byte[] { 0, 0, 0, 1 }, new byte[] { 3, 1, 2, 5, 4, 0 }, new byte[] { 1, 0, 0, 1, 0, 0 });

        moves = new State[] {moveU, moveL, moveR, moveB};

        // move tables
        tipsOm = new byte[N_TIPS_ORIENTATIONS][tipMoveNames.length];
        for (int i = 0; i < tipsOm.length; i++) {
            State state = new State(IndexMapping.indexToOrientation(i, 3, 4), new byte[4], new byte[6], new byte[6]);
            for (int j = 0; j < moves.length; j++) {
                tipsOm[i][j] = (byte) IndexMapping.orientationToIndex(state.multiply(tipMoves[j]).tipsOrientation, 3);
            }
        }

        verticesOm = new byte[N_VERTICES_ORIENTATIONS][moves.length];
        for (int i = 0; i < verticesOm.length; i++) {
            State state = new State(new byte[4], IndexMapping.indexToOrientation(i, 3, 4), new byte[6], new byte[6]);
            for (int j = 0; j < moves.length; j++) {
                verticesOm[i][j] = (byte) IndexMapping.orientationToIndex(state.multiply(moves[j]).verticesOrientation, 3);
            }
        }

        edgesPm = new short[N_EDGES_PERMUTATIONS][moves.length];
        for (int i = 0; i < edgesPm.length; i++) {
            State state = new State(new byte[4], new byte[4], IndexMapping.indexToEvenPermutation(i, 6), new byte[6]);
            for (int j = 0; j < moves.length; j++) {
                edgesPm[i][j] = (short) IndexMapping.evenPermutationToIndex(state.multiply(moves[j]).edgesPermutation);
            }
        }

        edgesOm = new byte[N_EDGES_ORIENTATIONS][moves.length];
        for (int i = 0; i < edgesOm.length; i++) {
            State state = new State(new byte[4], new byte[4], new byte[6], IndexMapping.indexToZeroSumOrientation(i, 2, 6));
            for (int j = 0; j < moves.length; j++) {
                edgesOm[i][j] = (byte) IndexMapping.zeroSumOrientationToIndex(state.multiply(moves[j]).edgesOrientation, 2);
            }
        }

        // prune tables
        tipsOd = new byte[N_TIPS_ORIENTATIONS];
        for (int i = 0; i < tipsOd.length; i++) {
            tipsOd[i] = -1;
        }
        tipsOd[0] = 0;

        int depth = 0;
        int nVisited;
        do {
            nVisited = 0;

            for (int i = 0; i < tipsOd.length; i++) {
                if (tipsOd[i] == depth) {
                    for (int k = 0; k < tipMoves.length; k++) {
                    	int y = i;
                    	for(int m = 0; m < 2; m++) {
                    		y = tipsOm[y][k];
                    		if (tipsOd[y] < 0) {
                                tipsOd[y] = (byte) (depth + 1);
                                nVisited++;
                            }
                    	}
                    }
                }
            }

            depth++;
        } while (nVisited > 0);

        verticesOd = new byte[N_VERTICES_ORIENTATIONS];
        for (int i = 0; i < verticesOd.length; i++) {
            verticesOd[i] = -1;
        }
        verticesOd[0] = 0;

        depth = 0;
        do {
            nVisited = 0;

            for (int i = 0; i < verticesOd.length; i++) {
                if (verticesOd[i] == depth) {
                    for (int k = 0; k < moves.length; k++) {
                    	int y = i;
                    	for(int m = 0; m < 2; m++) {
                    		y = verticesOm[y][k];
                    		if (verticesOd[y] < 0) {
                                verticesOd[y] = (byte) (depth + 1);
                                nVisited++;
                            }
                    	}
                    }
                }
            }

            depth++;
        } while (nVisited > 0);

        edgesPd = new byte[N_EDGES_PERMUTATIONS];
        for (int i = 0; i < edgesPd.length; i++) {
            edgesPd[i] = -1;
        }
        edgesPd[0] = 0;

        depth = 0;
        do {
            nVisited = 0;

            for (int i = 0; i < edgesPd.length; i++) {
                if (edgesPd[i] == depth) {
                    for (int k = 0; k < moves.length; k++) {
                    	int y = i;
                    	for(int m = 0; m < 2; m++) {
                    		y = edgesPm[y][k];
                    		if (edgesPd[y] < 0) {
                                edgesPd[y] = (byte) (depth + 1);
                                nVisited++;
                            }
                    	}
                    }
                }
            }

            depth++;
        } while (nVisited > 0);

        edgesOd = new byte[N_EDGES_ORIENTATIONS];
        for (int i = 0; i < edgesOd.length; i++) {
            edgesOd[i] = -1;
        }
        edgesOd[0] = 0;

        depth = 0;
        do {
            nVisited = 0;

            for (int i = 0; i < edgesOd.length; i++) {
                if (edgesOd[i] == depth) {
                    for (int k = 0; k < moves.length; k++) {
                    	int y = i;
                    	for(int m = 0; m < 2; m++) {
                    		y = edgesOm[y][k];
                    		if (edgesOd[y] < 0) {
                                edgesOd[y] = (byte) (depth + 1);
                                nVisited++;
                            }
                    	}
                    }
                }
            }

            depth++;
        } while (nVisited > 0);

        initialized = true;
    }

    private String[] solveTips(State state) {
        if (!initialized) {
            initialize();
        }

        int tipsOrientation =
            IndexMapping.orientationToIndex(state.tipsOrientation, 3);

        for (int depth = 0; ; depth++) {
            ArrayList<String> solution = new ArrayList<String>();
            if (searchTips(tipsOrientation, depth, solution, -1)) {
                String[] sequence = new String[solution.size()];
                solution.toArray(sequence);

                return sequence;
            }
        }
    }

    private boolean searchTips(int to, int depth, ArrayList<String> sol, int lv) {
        if (depth == 0) {
            return to == 0;
        }

        if (tipsOd[to] <= depth) {
            for (int i = 0; i < tipMoves.length; i++) {
                if (i == lv) {
                    continue;
                }
                int y = to;
                for (int j = 0; j < 2; j++) {
                	y = tipsOm[y][i];
                	if (searchTips(y, depth - 1, sol, i)) {
                		sol.add(tipMoveNames[i*2+j]);
                		return true;
                	}
                }
            }
        }

        return false;
    }

    private String[] solve(State state) {
        if (!initialized) {
            initialize();
        }

        int verticesOrientation =
            IndexMapping.orientationToIndex(state.verticesOrientation, 3);
        int edgesPermutation =
            IndexMapping.evenPermutationToIndex(state.edgesPermutation);
        int edgesOrientation =
            IndexMapping.zeroSumOrientationToIndex(state.edgesOrientation, 2);

        for (int depth = minScrambleLength; ; depth++) {
            ArrayList<String> solution = new ArrayList<String>();
            if (search(verticesOrientation, edgesPermutation, edgesOrientation, depth, solution, -1)) {
                String[] sequence = new String[solution.size()];
                solution.toArray(sequence);

                return sequence;
            }
        }
    }

    private boolean search(int vo, int ep, int eo, int depth,
            ArrayList<String> sol, int lv) {
        if (depth == 0) return vo == 0 && ep == 0 && eo == 0;

        if (verticesOd[vo] <= depth && edgesPd[ep] <= depth && edgesOd[eo] <= depth) {
            for (int i = 0; i < moves.length; i++) {
                if (i == lv) continue;

                int y = vo, s = ep, t = eo;
                for (int j = 0; j < 2; j++) {
                	y = verticesOm[y][i];
                	s = edgesPm[s][i];
                	t = edgesOm[t][i];
                	if (search(y, s, t, depth - 1, sol, i)) {
                		sol.add(moveNames[i*2+j]);
                		return true;
                	}
                }
            }
        }
        return false;
    }

    public String[] generate(State state) {
        HashMap<String, String> inverseMoveNames = new HashMap<String, String>();
        inverseMoveNames.put("u",  "u'");
        inverseMoveNames.put("u'", "u");
        inverseMoveNames.put("l",  "l'");
        inverseMoveNames.put("l'", "l");
        inverseMoveNames.put("r",  "r'");
        inverseMoveNames.put("r'", "r");
        inverseMoveNames.put("b",  "b'");
        inverseMoveNames.put("b'", "b");
        inverseMoveNames.put("U",  "U'");
        inverseMoveNames.put("U'", "U");
        inverseMoveNames.put("L",  "L'");
        inverseMoveNames.put("L'", "L");
        inverseMoveNames.put("R",  "R'");
        inverseMoveNames.put("R'", "R");
        inverseMoveNames.put("B",  "B'");
        inverseMoveNames.put("B'", "B");

        String[] solution = solve(state);

        HashMap<String, State> moves = new HashMap<String, State>();
        moves.put("U",  this.moves[0]);
        moves.put("U'", this.moves[0].multiply(this.moves[0]));
        moves.put("L",  this.moves[1]);
        moves.put("L'", this.moves[1].multiply(this.moves[1]));
        moves.put("R",  this.moves[2]);
        moves.put("R'", this.moves[2].multiply(this.moves[2]));
        moves.put("B",  this.moves[3]);
        moves.put("B'", this.moves[3].multiply(this.moves[3]));

        for (String move : solution) {
            state = state.multiply(moves.get(move));
        }

        String[] tipsSolution = solveTips(state);

        String[] sequence = new String[tipsSolution.length + solution.length];
        for (int i = 0; i < solution.length; i++) {
            sequence[i] = inverseMoveNames.get(solution[solution.length - 1 - i]);
        }
        for (int i = 0; i < tipsSolution.length; i++) {
            sequence[solution.length + i] = inverseMoveNames.get(tipsSolution[tipsSolution.length - 1 - i]);
        }

        return sequence;
    }

    public State getRandomState(Random random) {
        int tipsOrientation =
            random.nextInt(N_TIPS_ORIENTATIONS);
        int verticesOrientation =
            random.nextInt(N_VERTICES_ORIENTATIONS);
        int edgesPermutation =
            random.nextInt(N_EDGES_PERMUTATIONS);
        int edgesOrientation =
            random.nextInt(N_EDGES_ORIENTATIONS);

        return new State(
            IndexMapping.indexToOrientation(tipsOrientation, 3, 4),
            IndexMapping.indexToOrientation(verticesOrientation, 3, 4),
            IndexMapping.indexToEvenPermutation(edgesPermutation, 6),
            IndexMapping.indexToZeroSumOrientation(edgesOrientation, 2, 6));
    }
}
