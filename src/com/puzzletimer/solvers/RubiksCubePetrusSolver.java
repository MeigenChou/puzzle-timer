package com.puzzletimer.solvers;

import java.util.ArrayList;

import com.puzzletimer.solvers.RubiksCubeSolver.State;

public class RubiksCubePetrusSolver {
	private static short[][] epm = new short[1320][6];
	private static short[][] eom = new short[1760][6];
	private static byte[][] com = new byte[24][6];
	
	private static byte[] epd = new byte[1320];
	private static byte[] eod = new byte[1760];
	
	private static State[] moves = new State[6];

    static {
    	String[] turn = {"U", "D", "L", "R", "F", "B"};
        for (int i = 0; i < 6; i++) {
            moves[i] = State.moves.get(turn[i]);
        }
    }
    
	private static boolean ini=false;
	private static void init(){
		if(ini)return;
		byte[][] p = {
				{1,0,3,0,0,4},{2,1,1,5,1,0},{3,2,2,1,6,2},{0,3,7,3,2,3},
				{4,7,0,4,4,5},{5,4,5,6,5,1},{6,5,6,2,7,6},{7,6,4,7,3,7}
		};
		byte[][] o = {
				{0,0,1,0,0,2},{0,0,0,2,0,1},{0,0,0,1,2,0},{0,0,2,0,1,0},
				{0,0,2,0,0,1},{0,0,0,1,0,2},{0,0,0,2,1,0},{0,0,1,0,2,0}
		};
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 6; k++) {
					com[i*3+j][k]=(byte) (p[i][k]*3+(o[i][k]+j)%3);
				}
			}
		}
		for (int i = 0; i < 220; i++) {
			for (int j = 0; j < 8; j++) {
				State state = indicesToState(new int[]{i, j, j});
				for (int k = 0; k < 6; k++) {
					int[] ind = stateToIndices(state.multiply(moves[k]));
					eom[i*8+j][k] = (short)(ind[0]*8+ind[2]);
    				if(j<6)epm[i*6+j][k] = (short) (ind[0]*6+ind[1]);
				}
			}
		}
		for(int i=0; i<1320; i++) epd[i]=-1;
		epd[1158]=0;
		int d=0, c=0;
		for(d=0; d<5; d++){
			c=0;
			for(int i=0; i<1320; i++){
				if (epd[i] == d) {
					for (int j = 0; j < 6; j++) {
						for(int y = i, k = 0; k < 3; k++){
							y = epm[y][j];
							if (epd[y] < 0) {
								epd[y] = (byte) (d + 1);
								c++;
							}
						}
					}
				}
			}
			System.out.println(d+" "+c);
		}
		for(int i=0; i<1760; i++) eod[i]=-1;
		eod[1544]=0;
		for(d=0; d<5; d++){
			//c=0;
			for(int i=0; i<1760; i++){
				if (eod[i] == d) {
					for (int j = 0; j < 6; j++) {
						for(int y = i, k = 0; k < 3; k++){
							y = eom[y][j];
							if (eod[y] < 0) {
								eod[y] = (byte) (d + 1);
								//c++;
							}
						}
					}
				}
			}
			//System.out.println(d+" "+c);
		}
		ini=true;
	}
	
	protected static int[] stateToIndices(State state) {
		// edges
        boolean[] selectedEdges = {
            true,  false, false, false,
            false, false, false, false,
            true,  false, false, true,
        };
        byte[] edgesMapping = {
            0, -1, -1, -1,
           -1, -1, -1, -1,
            1, -1, -1,  2,
        };
        boolean[] edgesCombination = new boolean[12];
        for (int i = 0; i < 12; i++) {
            edgesCombination[i] = selectedEdges[state.edgesPermutation[i]];
        }
        int edgesCombinationIndex =
        	IndexMapping.combinationToIndex(edgesCombination, 3);
        byte[] edgesPermutation = new byte[3];
        byte[] edgesOrientation = new byte[3];
        int next = 0;
        for (int i = 0; i < 12; i++) {
            if (edgesCombination[i]) {
                edgesPermutation[next] = edgesMapping[state.edgesPermutation[i]];
                edgesOrientation[next++] = state.edgesOrientation[i];
            }
        }
        int edgesPermutationIndex =
            IndexMapping.permutationToIndex(edgesPermutation);
        int edgesOrientationIndex =
        	IndexMapping.orientationToIndex(edgesOrientation, 2);
        return new int[] {
            edgesCombinationIndex,
            edgesPermutationIndex,
            edgesOrientationIndex,
        };
    }

    private static State indicesToState(int[] indices) {
    	// edges
    	boolean[] combination =
    			IndexMapping.indexToCombination(indices[0], 3, 12);
    	byte[] permutation =
    			IndexMapping.indexToPermutation(indices[1], 3);
    	byte[] orientation =
    			IndexMapping.indexToOrientation(indices[2], 2, 3);

    	byte[] selectedEdges = { 0, 8, 11 };
    	int nextSelectedEdgeIndex = 0;
    	byte[] otherEdges = { 1, 2, 3, 4, 5, 6, 7, 9, 10 };
    	int nextOtherEdgeIndex = 0;

    	byte[] edgesPermutation = new byte[12];
    	byte[] edgesOrientation = new byte[12];
    	for (int i = 0; i < 12; i++) {
    		if (combination[i]) {
    			edgesPermutation[i] = selectedEdges[permutation[nextSelectedEdgeIndex]];
    			edgesOrientation[i] = orientation[nextSelectedEdgeIndex++];
    		} else {
    			edgesPermutation[i] = otherEdges[nextOtherEdgeIndex++];
    			edgesOrientation[i] = 0;
    		}
    	}

    	return new State(
    		new byte[8], new byte[8],
    		edgesPermutation,
    		edgesOrientation);
    }
    
    private static String[][] turn = {{"D","U","L","R","B","F"},
		{"F","B","L","R","D","U"},{"D","U","F","B","L","R"},
		{"D","U","R","L","F","B"},{"U","D","F","B","R","L"},
		{"U","D","L","R","F","B"},{"U","D","R","L","B","F"},
		{"U","D","B","F","L","R"}};
	private static String[] suff = {"","2","'"};
	private static void search(int co, int ep, int eo, int depth, int[] path,
			ArrayList<String> sol, int lm, int face) {
		if (depth == 0) {
			if(co==12 && ep==1158 && eo==1544) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < path.length; i++) {
                    sb.append(" "+turn[face][path[i]>>2]+suff[path[i]&3]);
                }
				sol.add(sb.toString());
			}
			return;
		}
		if (epd[ep] > depth || eod[eo] > depth) return;
		for (int i = 0; i < 6; i++) {
			if(i!=lm){
				int w=co, y=ep, s=eo;
				for(int j=0;j<3;j++){
					w=com[w][i];
					y=epm[y][i];s=eom[s][i];
					path[path.length - depth] = i << 2 | j;
					search(w, y, s, depth-1, path, sol, i, face);
				}
			}
		}
	}
	
	private static String[] moveIdx={"DULRBF","FBLRDU","DUFBLR","DURLFB",
		"UDFBRL","UDLRFB","UDRLBF","UDBFLR"};
	public static ArrayList<String> solve(String[] scr, int face){
		init();
		int co=12, ep=1158, eo=1544;
		for(int d=0;d<scr.length;d++){
			if(0!=scr[d].length()){
				int o=moveIdx[face].indexOf(scr[d].charAt(0));
				co=com[co][o];
				ep=epm[ep][o];eo=eom[eo][o];
				if(1<scr[d].length()) {
					if(scr[d].charAt(1)=='2'){
						co=com[co][o];eo=eom[eo][o];ep=epm[ep][o];
					} else {
						co=com[com[co][o]][o];eo=eom[eom[eo][o]][o];ep=epm[epm[ep][o]][o];
					}
				}
			}
		}
		ArrayList<String> sol = new ArrayList<String>();
		for(int d=0; ; d++){
			int[] path = new int[d];
			search(co,ep,eo,d,path,sol,-1,face);
			if(sol.size()>0) return sol;
		}
	}
}
