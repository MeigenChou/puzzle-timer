package com.puzzletimer.solvers;

import java.util.HashMap;

import com.puzzletimer.solvers.min2phase.Search;

public class RubiksCubeSolver {
    public static class State {
        public byte[] cornersPermutation;
        public byte[] cornersOrientation;
        public byte[] edgesPermutation;
        public byte[] edgesOrientation;

        public State(byte[] cornersPermutation, byte[] cornersOrientation, byte[] edgesPermutation, byte[] edgesOrientation) {
            this.cornersPermutation = cornersPermutation;
            this.cornersOrientation = cornersOrientation;
            this.edgesPermutation = edgesPermutation;
            this.edgesOrientation = edgesOrientation;
        }

        public State multiply(State move) {
            // corners
            byte[] cornersPermutation = new byte[8];
            byte[] cornersOrientation = new byte[8];

            for (int i = 0; i < 8; i++) {
                cornersPermutation[i] = this.cornersPermutation[move.cornersPermutation[i]];
                cornersOrientation[i] = (byte) ((this.cornersOrientation[move.cornersPermutation[i]] + move.cornersOrientation[i]) % 3);
            }

            // edges
            byte[] edgesPermutation = new byte[12];
            byte[] edgesOrientation = new byte[12];

            for (int i = 0; i < 12; i++) {
                edgesPermutation[i] = this.edgesPermutation[move.edgesPermutation[i]];
                edgesOrientation[i] = (byte) ((this.edgesOrientation[move.edgesPermutation[i]] + move.edgesOrientation[i]) % 2);
            }

            return new State(cornersPermutation, cornersOrientation, edgesPermutation, edgesOrientation);
        }

        public State applySequence(String[] sequence) {
            State state = this;
            for (String move : sequence) {
                state = state.multiply(moves.get(move));
            }

            return state;
        }

        public static HashMap<String, State> moves;

        static {
            State moveU = new State(new byte[] { 3, 0, 1, 2, 4, 5, 6, 7 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 7, 4, 5, 6, 8, 9, 10, 11 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            State moveD = new State(new byte[] { 0, 1, 2, 3, 5, 6, 7, 4 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 8 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            State moveL = new State(new byte[] { 4, 1, 2, 0, 7, 5, 6, 3 }, new byte[] { 2, 0, 0, 1, 1, 0, 0, 2 }, new byte[] { 11, 1, 2, 7, 4, 5, 6, 0, 8, 9, 10, 3 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            State moveR = new State(new byte[] { 0, 2, 6, 3, 4, 1, 5, 7 }, new byte[] { 0, 1, 2, 0, 0, 2, 1, 0 }, new byte[] { 0, 5, 9, 3, 4, 2, 6, 7, 8, 1, 10, 11 }, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            State moveF = new State(new byte[] { 0, 1, 3, 7, 4, 5, 2, 6 }, new byte[] { 0, 0, 1, 2, 0, 0, 2, 1 }, new byte[] { 0, 1, 6, 10, 4, 5, 3, 7, 8, 9, 2, 11 }, new byte[] { 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0 });
            State moveB = new State(new byte[] { 1, 5, 2, 3, 0, 4, 6, 7 }, new byte[] { 1, 2, 0, 0, 2, 1, 0, 0 }, new byte[] { 4, 8, 2, 3, 1, 5, 6, 7, 0, 9, 10, 11 }, new byte[] { 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 });

            moves = new HashMap<String, State>();
            moves.put("U",  moveU);
            moves.put("U2", moveU.multiply(moveU));
            moves.put("U'", moveU.multiply(moveU).multiply(moveU));
            moves.put("D",  moveD);
            moves.put("D2", moveD.multiply(moveD));
            moves.put("D'", moveD.multiply(moveD).multiply(moveD));
            moves.put("L",  moveL);
            moves.put("L2", moveL.multiply(moveL));
            moves.put("L'", moveL.multiply(moveL).multiply(moveL));
            moves.put("R",  moveR);
            moves.put("R2", moveR.multiply(moveR));
            moves.put("R'", moveR.multiply(moveR).multiply(moveR));
            moves.put("F",  moveF);
            moves.put("F2", moveF.multiply(moveF));
            moves.put("F'", moveF.multiply(moveF).multiply(moveF));
            moves.put("B",  moveB);
            moves.put("B2", moveB.multiply(moveB));
            moves.put("B'", moveB.multiply(moveB).multiply(moveB));
        }

        public static State id;

        static {
            id = new State(
                IndexMapping.indexToPermutation(0, 8),
                IndexMapping.indexToOrientation(0, 3, 8),
                IndexMapping.indexToPermutation(0, 12),
                IndexMapping.indexToOrientation(0, 2, 12));
        }
    }
    
    private static final byte[][] cornerFacelet = { {0, 36, 47}, {2, 45, 11}, {8, 9, 20}, {6, 18, 38},
    	{33, 53, 42}, {35, 17, 51}, {29, 26, 15}, {27, 44, 24} };
    private static final byte[][] edgeFacelet = { {50, 39}, {48, 14}, {23, 12}, {21, 41}, {1, 46}, {5, 10},
    	{7, 19}, {3, 37}, {34, 52}, {32, 16}, {28, 25}, {30, 43} };

    private static String toFaceCube(State cc) {
		char[] f = new char[54];
		char[] ts = {'U', 'R', 'F', 'D', 'L', 'B'};
		for (int i=0; i<54; i++) {
			f[i] = ts[i/9];
		}
		for (byte c=0; c<8; c++) {
			byte j = cc.cornersPermutation[c];
			byte ori = cc.cornersOrientation[c];
			for (byte n=0; n<3; n++)
				f[cornerFacelet[c][(n + ori) % 3]] = ts[cornerFacelet[j][n]/9];
		}
		for (byte e=0; e<12; e++) {
			byte j = cc.edgesPermutation[e];
			byte ori = cc.edgesOrientation[e];
			for (byte n=0; n<2; n++)
				f[edgeFacelet[e][(n + ori) % 2]] = ts[edgeFacelet[j][n]/9];
		}
		return new String(f);
	}

    public static String[] generate(State state) {
    	String cube = toFaceCube(state);
    	//System.out.println(cube);
    	Search search = new Search();
    	
        String[] solution = search.solution(cube, 21, 10000, 0, 2).split(" ");//solution(state);

        return solution;
    }
}
