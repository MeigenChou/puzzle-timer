package com.puzzletimer.solvers;

import java.util.ArrayList;
import java.util.Random;

public class RubiksDominoSolver {
	private static byte[] faces = {3, 3, 1, 1, 1, 1};

    private static char[][] epm = new char[40320][6];
	private static byte[] epd = new byte[40320];
	
	private static String[] turn = {"U", "D", "L", "R", "F", "B"};
    private static String[] suff = {"'", "2", ""};
    
    public static void cir(int[] arr, int a, int b){
    	int temp=arr[a]; arr[a]=arr[b]; arr[b]=temp;
    }
    
    static {
    	int[] arr = new int[8];
    	for (int i = 0; i < 40320; i++) {
    		for (int j = 0; j < 6; j++) {
    			TowerCubeSolver.set8Perm(arr, i);
    			switch(j){
    			case 0:TowerCubeSolver.cir(arr, 0, 3, 2, 1);break;	//U
    			case 1:TowerCubeSolver.cir(arr, 4, 5, 6, 7);break;	//D
    			case 2:cir(arr, 3, 7);break;	//L
    			case 3:cir(arr, 1, 5);break;	//R
    			case 4:cir(arr, 2, 6);break;	//F
    			case 5:cir(arr, 0, 4);break;	//B
    			}
    			epm[i][j]=(char) TowerCubeSolver.get8Perm(arr);
    		}
    	}

    	for (int i = 1; i < 40320; i++)
    		epd[i]=-1;
    	epd[0]=0;
    	
        for (int d = 0; d < 8; d++) {
        	for (int i = 0; i < 40320; i++)
        		if (epd[i] == d)
        			for (int k = 0; k < 6; k++)
        				for(int y = i, m = 0; m < faces[k]; m++) {
        					y = epm[y][k];
        					if (epd[y] < 0) {
        						epd[y] = (byte) (d + 1);
        					}
        				}
        }
    }

    private static boolean search(int cp, int ep, int depth, ArrayList<String> sol, int l){
    	if (depth == 0) return cp == 0 && ep == 0;
    	if (TowerCubeSolver.cpd[cp] > depth || epd[ep] > depth) return false;
    	int y, s;
    	for (int i = 0; i < 6; i++)
    		if(i != l){
    			y = cp; s = ep;
    			for(int k = 0; k < faces[i]; k++){
    				y = TowerCubeSolver.cpm[y][i]; s = epm[s][i];
    				if(search(y, s, depth - 1, sol, i)){
    					sol.add(turn[i] + (i<2?suff[k]:""));
    					return true;
    				}
    			}
    		}
    	return false;
    }

    public static String[] generate(Random r) {
    	int cp = r.nextInt(40320);
    	int ep = r.nextInt(40320);
    	
    	ArrayList<String> sol = new ArrayList<String>();
    	
    	for (int depth = 0; !search(cp, ep, depth, sol, -1); depth++);
    	
        String[] sequence = new String[sol.size()];
        sol.toArray(sequence);
        return sequence;
    }
}
