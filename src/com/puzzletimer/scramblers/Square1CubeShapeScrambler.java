package com.puzzletimer.scramblers;

import java.util.Random;

import com.puzzletimer.models.Scramble;
import com.puzzletimer.models.ScramblerInfo;
import com.puzzletimer.solvers.sq12phase.Search;
import com.puzzletimer.solvers.sq12phase.FullCube;

public class Square1CubeShapeScrambler implements Scrambler {
    private ScramblerInfo scramblerInfo;
    private Random random;
    private Search s = new Search();
    
    public Square1CubeShapeScrambler(ScramblerInfo scramblerInfo) {
        this.scramblerInfo = scramblerInfo;
        this.random = new Random();
    }

    @Override
    public ScramblerInfo getScramblerInfo() {
        return this.scramblerInfo;
    }

    @Override
    public Scramble getNextScramble() {
    	FullCube f = FullCube.randomCubeShape(this.random);
    	String[] scr = s.solution(f).split(" ");
        return new Scramble(
            getScramblerInfo().getScramblerId(), scr);
    }

    @Override
    public String toString() {
        return getScramblerInfo().getDescription();
    }
}
