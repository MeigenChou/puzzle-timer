package com.puzzletimer.scramblers;

import com.puzzletimer.models.Scramble;
import com.puzzletimer.models.ScramblerInfo;
import com.puzzletimer.solvers.sq12phase.Search;
import com.puzzletimer.solvers.sq12phase.FullCube;

public class Square1RandomScrambler implements Scrambler {
    private ScramblerInfo scramblerInfo;
    private Search s = new Search();
    
    public Square1RandomScrambler(ScramblerInfo scramblerInfo) {
        this.scramblerInfo = scramblerInfo;
    }

    @Override
    public ScramblerInfo getScramblerInfo() {
        return this.scramblerInfo;
    }

    @Override
    public Scramble getNextScramble() {
    	FullCube f = FullCube.randomCube();
    	String[] scr = s.solution(f).split(" ");
        return new Scramble(
            getScramblerInfo().getScramblerId(), scr);
    }

    @Override
    public String toString() {
        return getScramblerInfo().getDescription();
    }
}
